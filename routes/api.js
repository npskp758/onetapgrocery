
var express = require('express');
var router = express.Router();
var user_controller = require('../controllers/api/apiController');


//
//////////////////////////////////dashboard Api////////////////////////////
router.get('/dashboard', user_controller.dashboard);
//////////////////////////////////dashboard Api End////////////////////////////
//
//////////////////////////////////Admin Users Api////////////////////////////
router.post('/admin/add', user_controller.addAdmin);
router.get('/admin/addedit/:id?', user_controller.addeditAdmin);
router.get('/admin/list', user_controller.adminList);
router.get('/admin/delete/:id?', user_controller.deleteAdmin);
router.post('/adminmul/delete', user_controller.deleteAdminmul);
router.post('/searchProfileList', user_controller.SearchProfile);
//////////////////////////////////Admin Users Api End////////////////////////////
//
////////////////////////////// Account Setting Api/////////////////////////////////////////////
//
//router.get('/account_setting', user_controller.account_settingList);
router.get('/account_setting/addedit/:id?', user_controller.addeditAccount_setting);
router.post('/account_setting/add', user_controller.addAccount_setting);
router.post('/account_setting/fileupload', user_controller.fileupload);
//router.get('/account_setting/delete/:id?', user_controller.deleteAccount_setting);
//
//////////////////////////////////Account Setting Api ends////////////////////////////////////////
//
//////////////////////////////profile Api/////////////////////////////////////////////
//
// router.post('/profile/addedit', user_controller.addeditProfile);
router.post('/profile/add', user_controller.addProfile);
//
////////////////////////////////// Api ends////////////////////////////////////////
//
//////////////////////////////////Attribute Api////////////////////////////
router.post('/attribute/add', user_controller.addAttribute);
router.get('/attribute/addedit/:id?', user_controller.addeditAttribute);
router.get('/attribute', user_controller.attributeList);
router.get('/attribute/delete/:id?', user_controller.deleteAttribute);
router.post('/attributemul/delete', user_controller.deleteAttributemul);
//////////////////////////////////Attribute Api End////////////////////////////
//
//
//////////////////////////////////Roles Api////////////////////////////
// router.post('/roles/add', user_controller.addRoles);
//router.get('/roles/addedit/:id?', user_controller.addeditRoles);
//router.get('/roles', user_controller.rolesList);
// router.get('/roles/delete/:id?', user_controller.deleteRoles);
router.post('/RolesMul/delete', user_controller.RolesMulDelete);
router.post('/searchRoleList', user_controller.SearchRoles);
//////////////////////////////////Roles Api End////////////////////////////
//
//
//////////////////////////////////Citi Api////////////////////////////
// router.post('/citi/add', user_controller.addCiti);
// router.get('/citi/addedit/:id?', user_controller.addeditCiti);
// router.get('/citi', user_controller.citiList);
// router.get('/citi/delete/:id?', user_controller.deleteCiti);
router.post('/citymul/delete', user_controller.deleteCitymul);
//////////////////////////////////Citi Api End////////////////////////////
//
//
//////////////////////////////////Zipcode Api////////////////////////////
// router.post('/zipcode/add', user_controller.addZipcode);
// router.get('/zipcode/addedit/:id?', user_controller.addeditZipcode);
// router.get('/zipcode', user_controller.zipcodeList);
// router.get('/zipcode/delete/:id?', user_controller.deleteZipcode);
router.post('/zipcodemul/delete', user_controller.deleteZipcodemul);
//////////////////////////////////Zipcode Api End////////////////////////////
//
//
//////////////////////////////////Stores Api////////////////////////////
router.post('/storemul/delete', user_controller.deleteStoreemul);
//////////////////////////////////Stores Api End////////////////////////////
//
//
//////////////////////////////////Category Api////////////////////////////
// router.post('/category/add', user_controller.addCategory);
// router.post('/category/fileupload', user_controller.fileupload);
// router.get('/category/addedit/:id?', user_controller.addeditCategory);
// router.get('/category', user_controller.categoryList);
// router.get('/category/delete/:id?', user_controller.deleteCategory);
router.post('/categorymul/delete', user_controller.deleteCategorymul);
//////////////////////////////////Category Api End////////////////////////////
//
//
//////////////////////////////////Category Api////////////////////////////
router.post('/subcategory2', user_controller.subcatagoryListbycatagory);
router.post('/subcategory/add', user_controller.addSubcategory);
router.post('/category/fileupload', user_controller.fileupload);
router.get('/subcategory/addedit/:id?', user_controller.addeditSubcategory);
router.get('/subcategory', user_controller.subcategoryList);
router.get('/subcategory/delete/:id?', user_controller.deleteSubcategory);
router.post('/subcategorymul/delete', user_controller.deleteSubcategorymul);
//////////////////////////////////SubCategory Api End////////////////////////////
//
router.post('/subcategory/product-list',user_controller.productListBySubCategory);


//
//////////////////////////////////Product Api////////////////////////////
// router.post('/product/add', user_controller.addProduct);
// router.post('/product/fileupload', user_controller.fileupload);
// router.get('/product/addedit/:id?', user_controller.addeditProduct);
// router.get('/product', user_controller.productList);
// router.get('/product/delete/:id?', user_controller.deleteProduct);
// router.post('/productmul/delete', user_controller.deleteProductmul);
//////////////////////////////////Product Api End////////////////////////////
//
//////////////////////////////////oderitem Api////////////////////////////
router.post('/order_item/add', user_controller.order_item);
router.get('/order_item', user_controller.order_itemList);
router.get('/orderitem/addedit/:id?', user_controller.addeditOderitem);
// router.post('/orderitem/add', user_controller.addOrderitem);
// router.post('/orderitemmul/delete', user_controller.deleteOrdermul);

//////////////////////////////////oderitem Api End////////////////////////////
//

//////////////////////////////////Customer Api////////////////////////////
router.post('/customer/add', user_controller.addCustomer);
//router.post('/customer/fileupload', user_controller.fileupload);
router.get('/customer/addedit/:id?', user_controller.addeditCustomer);
//router.get('/customer', user_controller.customerList);
//router.get('/customer/delete/:id?', user_controller.deleteCustomer);
//router.post('/customermul/delete', user_controller.deleteCoustomermul);
//////////////////////////////////Customer Api End////////////////////////////
//
//
//////////////////////////////////Customer_fav Api////////////////////////////
router.post('/customer_fav/add', user_controller.addCustomer_fav);
//router.post('/customer/fileupload', user_controller.fileupload);
router.get('/customer_fav/addedit/:id?', user_controller.addeditCustomer_fav);
router.get('/customer_fav', user_controller.customer_favList);
router.get('/customer_fav/delete/:id?', user_controller.deleteCustomer_fav);
//////////////////////////////////Customer_fav Api End////////////////////////////
//
//////////////////////////////////Customer_group Api////////////////////////////
router.post('/customer_group/add', user_controller.addCustomer_group);
router.get('/customer_group/addedit/:id?', user_controller.addeditCustomer_group);
router.get('/customer_group', user_controller.customer_groupList);
router.get('/customer_group/delete/:id?', user_controller.deleteCustomer_group);
router.post('/customergrpmul/delete', user_controller.deleteCoustomerGroupmul);
//////////////////////////////////Customer_group Api End////////////////////////////
//
//////////////////////////////////Customer Address Api////////////////////////////
//router.post('/customer_address/add', user_controller.customer_address);
//router.get('/customer_address', user_controller.customer_addressList);
//////////////////////////////////Customer Address Api End////////////////////////////
//
//////////////////////////////////Customer Address Api////////////////////////////
router.post('/customer_address/add', user_controller.addCustomer_address);
router.get('/customer_address', user_controller.customer_addressList);
router.get('/customer_address/addedit/:id?', user_controller.addeditCustomer_address);
router.get('/customer_address/delete/:id?', user_controller.deleteCustomer_address);
// router.post('/customer_addressmul/delete', user_controller.deleteCustomer_addressmul);
//////////////////////////////////Customer Address Api End////////////////////////////
//
//////////////////////////////////Order Api////////////////////////////
router.get('/order', user_controller.orderList);
router.get('/order/addedit/:id?', user_controller.addeditOrder);
router.post('/order/add', user_controller.addOrder);
router.get('/order/delete/:id?', user_controller.deleteOrder);
router.get('/customerBymobile/:mobile?', user_controller.customerBymobile);
router.get('/product_Subcat_Bycategory/:id?', user_controller.product_Subcat_Bycategory);
router.post('/oredermul/delete', user_controller.deleteoredermulsmul);

//////////////////////////////////Order Api End////////////////////////////
//
//
//////////////////////////////////Order Status Api////////////////////////////
router.get('/order_status_history', user_controller.order_status_historyList);
router.post('/order_status_history/add', user_controller.addOrder_status_history);
router.get('/order_status_history/addedit/:id?', user_controller.addeditOrder_status_history);
router.get('/order_status_history/delete/:id?', user_controller.deleteOrder_status_history);
router.post('/order_status_historyMul/delete', user_controller.order_status_historyMul);
//////////////////////////////////Order Status Api End////////////////////////////
//
//
//////////////////////////////////CMS Api////////////////////////////
router.get('/cms', user_controller.cmsList);
router.post('/cms/add', user_controller.addCms);
router.get('/cms/addedit/:id?', user_controller.addeditCms);
router.get('/cms/delete/:id?', user_controller.deleteCms);
router.post('/cmsmul/delete', user_controller.deleteCmsMul)
//////////////////////////////////CMS Api End////////////////////////////
//
//
//////////////////////////////////Notification Api////////////////////////////
router.get('/notification', user_controller.notificationList);
router.post('/notification/add', user_controller.addNotification);
router.get('/notification/addedit/:id?', user_controller.addeditNotification);
router.get('/notification/delete/:id?', user_controller.deleteNotification);
router.post('/notificationmul/delete', user_controller.deleteNotificationMul)
//////////////////////////////////notification Api End////////////////////////////
//
//
//////////////////////////////////Notification_trans Api////////////////////////////
router.get('/notification_trans', user_controller.notification_transList);
router.post('/notification_trans/add', user_controller.addNotification_trans);
router.get('/notification_trans/addedit/:id?', user_controller.addeditNotification_trans);
router.get('/notification_trans/delete/:id?', user_controller.deleteNotification_trans);
router.post('/notification_trans_mul/delete', user_controller.deleteNotification_transMul)
//////////////////////////////////Notification_trans Api End////////////////////////////
//
//
//////////////////////////////////Settings profile  Api////////////////////////////
router.get('/settings_profile', user_controller.settings_profileList);
router.get('/settings_profile/addedit/:id?', user_controller.addeditSettings_profile);
router.post('/settings_profile/add', user_controller.addSettings_profile);
router.get('/settings_profile/delete/:id?', user_controller.deleteSettings_profile);
router.post('/settings_profileMul/delete', user_controller.deleteSettings_profileMul);

//////////////////////////////////Settings profile Api End////////////////////////////
//
//
//////////////////////////////////delivery_time_slot Api////////////////////////////
// router.post('/delivery_time_slot/add', user_controller.addDelivery_time_slot);
// router.get('/delivery_time_slot', user_controller.delivery_time_slotList);
// router.get('/delivery_time_slot/addedit/:id?', user_controller.addeditDelivery_time_slot);
// router.get('/delivery_time_slot/delete/:id?', user_controller.deleteDelivery_time_slot);
router.post('/delivery_time_slot/delete', user_controller.deleteDelivery_time_slotmul);
//////////////////////////////////delivery_time_slot Api End////////////////////////////
//
//
//////////////////////////////////wallet Api////////////////////////////
// router.post('/wallet/add', user_controller.addWallet);
// router.get('/wallet', user_controller.walletList);
// router.get('/wallet/addedit/:id?', user_controller.addeditWallet);
// router.get('/wallet/delete/:id?', user_controller.deleteWallet);
// router.post('/walletmul/delete', user_controller.deleteWalletMul);
//////////////////////////////////wallet Api End////////////////////////////
//
//
//////////////////////////////////banner Api////////////////////////////
//router.post('/banner/add', user_controller.addBanner);
//router.post('/banner/fileupload', user_controller.fileupload);
//router.get('/banner/addedit/:id?', user_controller.addeditBanner);
//router.get('/banner', user_controller.bannerList);
// router.get('/banner/delete/:id?', user_controller.deleteBanner);
//router.post('/bannermul/delete', user_controller.deleteBannermul);
//////////////////////////////////banner Api End////////////////////////////
//
//
//////////////////////////////////banner_section Api////////////////////////////
// router.post('/banner_section/add', user_controller.addBanner_section);
// router.get('/banner_section', user_controller.banner_sectionList);
// router.get('/banner_section/addedit/:id?', user_controller.addeditBanner_section);
// router.get('/banner_section/delete/:id?', user_controller.deleteBanner_section);
router.post('/banner_sectionmul/delete', user_controller.deleteBanner_sectionmul);
//////////////////////////////////banner_section Api End////////////////////////////
//
//
//////////////////////////////////banner_display Api////////////////////////////
// router.post('/banner_display/add', user_controller.addBanner_display);
// router.get('/banner_display', user_controller.banner_displayList);
// router.get('/banner_display/addedit/:id?', user_controller.addeditBanner_display);
// router.get('/banner_display/delete/:id?', user_controller.deleteBanner_display);
router.post('/banner_displaymul/delete', user_controller.deleteBanner_displaymul);
//////////////////////////////////banner_display Api End////////////////////////////
//
//
//////////////////////////////////salesman Api////////////////////////////
// router.post('/salesman/add', user_controller.addSalesman);
// router.get('/salesman', user_controller.salesmanList);
// router.get('/salesman/addedit/:id?', user_controller.addeditSalesman);
router.get('/salesman/delete/:id?', user_controller.deleteSalesman);
router.post('/salesmanmul/delete', user_controller.deleteSalesmanmul);
//////////////////////////////////salesman Api End////////////////////////////
//
//////////////////////////////////dropdown_settings Api////////////////////////////
// router.post('/dropdown_settings/add', user_controller.addDropdown_settings);
// router.get('/dropdown_settings/addedit/:id?', user_controller.addeditDropdown_settings);
// router.get('/dropdown_settings', user_controller.dropdown_settingsList);
// router.get('/dropdown_settings/delete/:id?', user_controller.deleteDropdown_settings);
router.post('/dropdown_settingsmul/delete', user_controller.deleteDropdown_settingsmul);
//////////////////////////////////dropdown_settings Api End////////////////////////////
//
//
////////////////////////////////// Users Api////////////////////////////
// router.post('/users/add', user_controller.addUser);
// router.get('/users/addedit/:id?', user_controller.addeditUser);
// router.get('/users', user_controller.usersList);
// router.get('/users/delete/:id?', user_controller.deleteUsers);
router.post('/usermul/delete', user_controller.deleteUsersmul);
// router.post('/usersUqEmail', user_controller.usersUqEmail);
// router.post('/users/fileupload', user_controller.fileupload);
////////////////////////////////// Users Api End////////////////////////////
//

//////////////////////////////////shipping_method Api////////////////////////////
// router.post('/shipping_method/add', user_controller.addShipping_method);
// router.get('/shipping_method', user_controller.shipping_methodList);
// router.get('/shipping_method/addedit/:id?', user_controller.addeditShipping_method);
// router.get('/shipping_method/delete/:id?', user_controller.deleteShipping_method);
//////////////////////////////////shipping_method Api End////////////////////////////



router.get('/', user_controller.index);
router.get('/getheader', user_controller.getheader);
router.get('/gethomeicon', user_controller.gethomeicon);
router.get('/getmodulenoti/:table_name?', user_controller.getmodulenoti);


router.get('/getBanner/:slug?', user_controller.getbanner);
router.get('/getCalltoAction/:slug?', user_controller.getcalltoaction);
router.get('/roledetails/:id?', user_controller.roledetails);

router.get('/updatenotification/:user_id?/:module_id?/:table_name?', user_controller.updatenotification);
router.get('/saveactivity/:type?/:module_id?/:prod_id?', user_controller.saveactivity);
router.get('/claimowner/:project_id?', user_controller.claimowner);

router.get('/myprojects/:module_id?/:user_id?', user_controller.myprojects);
router.post('/jobpost', user_controller.jobpost);
router.post('/adpost', user_controller.adpost);
router.post('/adwrite', user_controller.adwrite);

router.post('/login', user_controller.login);
router.post('/loginNew', user_controller.loginNew);
router.post('/userlogin', user_controller.userlogin);
router.get('/userdetails', user_controller.userdetails);
router.get('/dropdown/:slug?', user_controller.getDropdown);
router.post('/changePassword', user_controller.changePassword);
router.post('/otpCheack', user_controller.otpCheack);
router.post('/register', user_controller.register);
router.post('/newRegister', user_controller.newRegister);

router.post('/settings_profile', user_controller.settings_profileList);
// router.post('/category', user_controller.categoryList);
// router.post('/product', user_controller.productList);
router.post('/upload', user_controller.upload);

router.post('/appProductList', user_controller.appProductList);

//router.get('/rolesList', user_controller.rolesList);
// router.get('/productList', user_controller.productList);
router.post('/appCategoryList', user_controller.appCategoryList);
router.post('/appBannerList', user_controller.appBannerList);
router.post('/appSliderBannerList', user_controller.appSliderBannerList);

router.post('/appCustomerAdd', user_controller.appCustomerAdd);
router.post('/appCustomerAddressAdd', user_controller.appCustomerAddressAdd);
router.post('/appCustomerAddress', user_controller.appCustomerAddress);
router.post('/appCustomerAddressEdit', user_controller.appCustomerAddressEdit);
router.post('/appCustomerAddressDelete', user_controller.appCustomerAddressDelete);
router.post('/appCustomerAddressPrime', user_controller.appCustomerAddressPrime);

router.post('/appCartAdd', user_controller.appCartAdd);
router.post('/appCartDelete', user_controller.appCartDelete);
router.post('/appCartListing', user_controller.appCartListing);
router.post('/appCartItemAdd', user_controller.appCartItemAdd);
router.post('/appCartItemRemove', user_controller.appCartItemRemove);
router.post('/appMultipleCartItemAdd', user_controller.appMultipleCartItemAdd);

router.post('/appForgotPassword', user_controller.appForgotPassword);
router.post('/appResetPassword', user_controller.appResetPassword);
router.post('/appChangePassword', user_controller.appChangePassword);

router.post('/appShowProfile', user_controller.appShowProfile);
router.post('/appUpdateProfile', user_controller.appUpdateProfile);

router.post('/appProductFilter', user_controller.appProductFilter);

router.post('/appOrder', user_controller.appOrder);
router.post('/appOrderCancel/:orderId', user_controller.appOrderCancel);
router.post('/appCustomerOrder', user_controller.appCustomerOrder);

router.post('/appFeedback', user_controller.appFeedback);

router.post('/appGlobalSearch', user_controller.appGlobalSearch);
router.post('/appGlobalSearchSuggestion1', user_controller.appGlobalSearchSuggestion1);
router.post('/appGlobalSearchSuggestion', user_controller.appGlobalSearchSuggestion);

router.post('/appExpressShipping', user_controller.appExpressShipping);

router.get('/emailCheck', user_controller.emailCheck);

router.post('/appSiteSettings', user_controller.appSiteSettings);

router.post('/appOtp', user_controller.appOtp);

router.post('/appWalletDashboard', user_controller.appWalletDashboard);
router.post('/appWalletTransaction', user_controller.appWalletTransaction);

router.post('/appCouponCode', user_controller.appCouponCode);

router.post('/genarateChecksum',  user_controller.genarateChecksum);

router.post('/appCheckOtp',  user_controller.appCheckOtp);

router.get('/appPinCodeList', user_controller.appPinCodeList);

router.get('/appVersionCheck', user_controller.appVersionCheck);


//************************************************************
module.exports = router;
