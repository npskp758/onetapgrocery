/*Project: super_pos
 * 
 * 
 * 
* */
var express 			= require('express');
var path 				= require('path');
var favicon 			= require('static-favicon');
var logger 				= require('morgan');
var cookieParser 		= require('cookie-parser');
var bodyParser 			= require('body-parser');
var flash             	= require('connect-flash');
// const toastr            = require('express-toastr');
var crypto            	= require('crypto');
var sess              	= require('express-session');
var Store             	= require('express-session').Store;
var bcrypt 				= require('bcrypt-nodejs');
var partials 			= require('express-partial');
var csrf 				= require('csurf');
var cors 				= require('cors');
var moment              = require('moment');
var expressValidator    = require('express-validator');
var models 				= require('./models'); 
///Express session store and expiration
var store               = require('session-memory-store')(sess);
// var flash = require('connect-flash');
const paginate = require('express-paginate');
var app = express();

//app.configure(function() {
  //app.use(express.cookieParser('keyboard cat'));
  //app.use(express.session({ cookie: { maxAge: 60000 }}));
 // app.use(flash());
//});

// keep this before all routes that will use pagination
app.use(paginate.middleware(10, 50));

app.set('port', process.env.PORT || 3302);
var server = app.listen(app.get('port'), function() {	
	models.sequelize.sync().then(() => {
        console.log('model load');
    })
    .catch(function (e) {
        throw new Error(e);
    });
	console.log('Express server listening on port ' + app.get('port'));
  //debug('Express server listening on port ' + server.address().port);
});

///variable declare
app.locals.moment = moment;
app.locals.adminbaseurl='http://localhost:'+app.get('port')+'/superpos/';
app.locals.baseurl='http://localhost:'+app.get('port')+'/';
app.locals.apiurl='http://localhost:'+app.get('port')+'/api/v1/';
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));   
app.use(flash());
app.use(sess({
    name: 'nodescratch',
    secret: 'MYSECRETISVERYSECRET',
    //store:  new store({ expires: 60 * 60 * 1000, debug: true }),
    store:  new store({ maxAge:null, debug: true }),
    resave: true,
    saveUninitialized: true
}));   

///routes
var routes 				= require('./routes/index');
var users 				= require('./routes/users');
var api 				= require('./routes/api');
var superpos 		    = require('./routes/superpos');
///routes end

app.use(cors());
app.use('/', routes);
app.use('/api/v1', api);
app.use('/superpos', superpos);


/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator()); // Add this after the bodyParser middlewares!

module.exports = app;