module.exports = function(sequelize, DataTypes) {
    return sequelize.define('customer', {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      first_name: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      last_name: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      email: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      phone: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      password: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      dob: {
        type: DataTypes.DATEONLY,
        allowNull: true
      },
      doa: {
        type: DataTypes.DATEONLY,
        allowNull: true
      },
      gender: {
        type: DataTypes.ENUM('male','female'),
        allowNull: true
      },
      group_id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: true,
      },
      status: {
        type: DataTypes.ENUM('active','inactive'),
        allowNull: true
      },
      image: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      otp: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: true,
      },
      otp_created_at: {
        type: DataTypes.DATE,
        allowNull: true
      },
      sponsor_code: {
        type: DataTypes.STRING(128),
        allowNull: true
      }, 
      used_sponsor_code: {
        type: DataTypes.STRING(128),
        allowNull: true
      }, 
      is_approved: {
        type: DataTypes.INTEGER(2).UNSIGNED,
        allowNull: true,
      },
    }, {
      tableName: 'customer'
    });
  };
  