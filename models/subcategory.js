module.exports = function(sequelize, DataTypes) {
    return sequelize.define('subcategory', {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      store_id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false
      },
      category: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
	    title: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
	    icon: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      // banner_icon: {
      //   type: DataTypes.TEXT,
      //   allowNull: true
      // },
      status: {
        type: DataTypes.ENUM('active','inactive','archive'),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'subcategory'
    });
  };
  