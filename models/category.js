module.exports = function(sequelize, DataTypes) {
  return sequelize.define('category', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    store_id: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    // parent_category_id: {
    //   type: DataTypes.INTEGER(10).UNSIGNED,
    //   allowNull: false,
    //   defaultValue:0
    // },
    parent_category_id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true,
    },
    position: {
      type: DataTypes.INTEGER(5).UNSIGNED,
      allowNull: true
    },
    path: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    url: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    icon: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    // product_count:{
    //   type: DataTypes.DECIMAL(12,4),
    //   defaultValue:0.0000
    // },
    product_count:{
        type: DataTypes.INTEGER(10),
        allowNull: true
      },
    include_in_menu: {
      type: DataTypes.ENUM('yes','no'),
      allowNull: true
    },
    meta_title: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    meta_key: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    meta_description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    status: {
      type: DataTypes.ENUM('active','inactive','archive'),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdBy: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedBy: {
      type: DataTypes.STRING(128),
      allowNull: true
    }
  }, {
    tableName: 'category'
  });
};
  