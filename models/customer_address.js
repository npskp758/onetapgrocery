module.exports = function(sequelize, DataTypes) {
    return sequelize.define('customer_address', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
      },
      customer_id: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: true,
      },
      // is_primary: {
      //   type: DataTypes.ENUM('yes','no'),
      //   allowNull: true
      // },
      // name: {
      //   type: DataTypes.STRING(255),
      //   allowNull: true
      // },
      first_name: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      last_name: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      is_prime_address: {
        type: DataTypes.INTEGER(1),
        allowNull: true
      },
      mobile: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      // street1: {
      //   type: DataTypes.TEXT,
      //   allowNull: true
      // },
      // street2: {
      //   type: DataTypes.TEXT,
      //   allowNull: true
      // },
      address: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      city: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      state: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      pin: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      country: {
        type: DataTypes.ENUM('india'),
        allowNull: true
      },
      // lat: {
      //   type: DataTypes.STRING(255),
      //   allowNull: true
      // },
      // long: {
      //   type: DataTypes.STRING(255),
      //   allowNull: true
      // },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'customer_address'
    });
  };
  