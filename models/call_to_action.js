/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('call_to_action', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      title: {
        type: DataTypes.STRING(300),
        allowNull: false
      },
      description : {
        type: DataTypes.STRING(1000),
        allowNull: false
      },
      backgroungImage: {
        type: DataTypes.STRING(300),
        allowNull: true
      },
      image: {
        type: DataTypes.STRING(200),
        allowNull: false
      },
      moduleId: {
        type: DataTypes.STRING(200),
        allowNull: false
      }
    }, {
      tableName: 'call_to_action'
    });
};