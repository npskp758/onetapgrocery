/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('modules_role', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    modulesId: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    roleId: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    isView: {
      type: DataTypes.ENUM('Y','N'),
      allowNull: true
    },
    isCreate: {
      type: DataTypes.ENUM('Y','N'),
      allowNull: true
    },
    isUpdate: {
      type: DataTypes.ENUM('Y','N'),
      allowNull: true
    },
    isDelete: {
      type: DataTypes.ENUM('Y','N'),
      allowNull: true
    },
    // status: {
    //   type: DataTypes.ENUM('isView','isCreate','isUpdate','isDelete'),
    //   allowNull: true
    // },
    createdBy: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    updatedBy: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'modules_role'
  });
};
