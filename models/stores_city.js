module.exports = function(sequelize, DataTypes) {
    return sequelize.define('stores_city', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
      },
      stores_id: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: true,
      },
      stores_city_id: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      stores_city_name: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    }, {
      tableName: 'stores_city'
    });
  };
  