/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('wallet', {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    cid: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
    },  
    sponsor_code: {
      type: DataTypes.STRING(128),
      allowNull: true
    },   
    amount: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: false,
      defaultValue: 0.00
    },
    amount_type: {
      type: DataTypes.ENUM('debit','credit'),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },        
    status: {
      type: DataTypes.ENUM('active','inactive','archive'),
      allowNull: true
    }
  }, {
    tableName: 'wallet'
  });
};