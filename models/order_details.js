module.exports = function(sequelize, DataTypes) {
  return sequelize.define('order_details', {
    id: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    order_details: {
      type: DataTypes.TEXT(),
      allowNull: true
    }
  }, {
    tableName: 'order_details'
  });
};
