module.exports = function(sequelize, DataTypes) {
    return sequelize.define('customer_group', {
      id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      label: {
        type: DataTypes.STRING(32),
        allowNull: true
      },
      status: {
        type: DataTypes.ENUM('active','inactive','archive'),
        allowNull: true
      },
    }, {
      tableName: 'customer_group'
    });
  };
  