module.exports = function(sequelize, DataTypes) {
    return sequelize.define('feedback', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
      },
      phoneno: {
        type: DataTypes.INTEGER(11).UNSIGNED,
        allowNull: true,
      },
      message: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    }, {
      tableName: 'sms'
    });
  };
  