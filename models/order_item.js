module.exports = function(sequelize, DataTypes) {
    return sequelize.define('order_item', {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      order_id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false
      },
      store_id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false
      },
      product_id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      sku: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      description: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      discount_percent: {
        type: DataTypes.DECIMAL(12,4),
        allowNull: true,
        defaultValue: 0.0000
      },
      discount_amount: {
        type: DataTypes.DECIMAL(12,4),
        allowNull: true,
        defaultValue: 0.0000
      },
      qty: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: true,
        defaultValue: 0
      },
      price: {
        type: DataTypes.DECIMAL(12,4),
        allowNull: true,
        defaultValue: 0.0000
      },
      original_price: {
        type: DataTypes.DECIMAL(12,4),
        allowNull: true,
        defaultValue: 0.0000
      },
      cgst: {
        type: DataTypes.DECIMAL(12,4),
        allowNull: true,
        defaultValue: 0.0000
      },
      sgst: {
        type: DataTypes.DECIMAL(12,4),
        allowNull: true,
        defaultValue: 0.0000
      },
      igst: {
        type: DataTypes.DECIMAL(12,4),
        allowNull: true,
        defaultValue: 0.0000
      },
      row_total: {
        type: DataTypes.DECIMAL(12,4),
        allowNull: true,
        defaultValue: 0.0000
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'order_item'
    });
  };
  