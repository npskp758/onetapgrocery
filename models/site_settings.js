/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('site_settings', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    site_name: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    mobile_no: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    app_version: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    fax: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    site_url: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    address: {
      type: DataTypes.TEXT(),
      allowNull: true
    },
    feature: {
      type: DataTypes.TEXT(),
      allowNull: true
    },
    max_pro_availability: {
      type: DataTypes.INTEGER(10).UNSIGNED,
      allowNull: true
    },
    shipping_charges: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00 
    },
    free_shipping_limit: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00 
    },
    createdBy: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'site_settings'
  });
};
