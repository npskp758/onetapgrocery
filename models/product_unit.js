module.exports = function(sequelize, DataTypes) {
  return sequelize.define('product_unit', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    sku: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    product_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true,
    },
    size: {
      type: DataTypes.STRING(255),
      allowNull: true 
    },
    price: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00 
    },
    special_price: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00 
    },    
    quantity: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true,
      defaultValue: 0
    },    
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'product_unit'
  });
};
  