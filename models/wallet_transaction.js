/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('wallet_transaction', {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    cid: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
    }, 
    transcation_type: {
      type: DataTypes.ENUM('addmoney','purchase','cashback'),
      allowNull: true
    },   
    amount: {
      type: DataTypes.DECIMAL(12,4),
      allowNull: false,
      defaultValue: 0.0000
    },
    amount_type: {
      type: DataTypes.ENUM('debit','credit'),
      allowNull: true
    },
    remarks: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    order_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true,
    }, 
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },        
    status: {
      type: DataTypes.ENUM('active','inactive','archive'),
      allowNull: true
    }
  }, {
    tableName: 'wallet_transaction'
  });
};
