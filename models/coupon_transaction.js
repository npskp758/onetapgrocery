/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('coupon_transaction', {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    customer_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true,
    }, 
    applied_amount: {
      type: DataTypes.DECIMAL(12,2),
      allowNull: true,
      defaultValue: 0.00
    },
    coupon_type: {
      type: DataTypes.ENUM('amount','percentage'),
      allowNull: true
    },
    coupon_value: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    date_from: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    date_to: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    time_from: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    time_to: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    coupon_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    purchase_limit: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    createdBy: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    is_share: {
      type: DataTypes.ENUM('yes','no'),
      allowNull: true
    },
    status: {
      type: DataTypes.ENUM('active','inactive','archive'),
      allowNull: true
    }
  }, {
    tableName: 'coupon_transaction'
  });
};
