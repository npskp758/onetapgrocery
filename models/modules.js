/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('modules', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    slag: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    usefor: {
      type: DataTypes.ENUM('header','footer','nineicons','none'),
      allowNull: true
    },
    relatedTableName: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    menuIcon: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    menuHoverIcon: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    homeIcon: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    bannerImage: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    bannerIcon: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    subTitle: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    shortDesc: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    desc: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    footerImage1: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    footerImage2: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    footerContent1: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    footerContent2: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    metaTitle: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    metaDesc: {
      type: DataTypes.STRING(1000),
      allowNull: true
    },
    metaKeyword: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    sequence: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    createdBy: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    updatedBy: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    status: {
      type: DataTypes.ENUM('active','inactive','archive'),
      allowNull: true
    }
  }, {
    tableName: 'modules'
  });
};
