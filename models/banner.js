module.exports = function(sequelize, DataTypes) {
    return sequelize.define('banner', {
      id: {
        type: DataTypes.INTEGER(11),
        allowNull: true,
        primaryKey: true,
        autoIncrement: true
      },
      title: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      category_id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: true
      },
      store_id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: true
      },
      short_description: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      type: {
        type: DataTypes.ENUM('Advertise','Slider'),
        allowNull: true
      },
      image: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      url: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      section: {
        type: DataTypes.INTEGER(3).UNSIGNED,
        allowNull: true
      },
      display_type: {
        type: DataTypes.INTEGER(3).UNSIGNED,
        allowNull: true
      },
      // row: {
      //   type: DataTypes.INTEGER(11).UNSIGNED,
      //   allowNull: true
      // },
      sequence: {
        type: DataTypes.INTEGER(3).UNSIGNED,
        allowNull: true
      },
      status: {
        type: DataTypes.ENUM('active','inactive','archive'),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'banner'
    });
  };
  