module.exports = function(sequelize, DataTypes) {
    return sequelize.define('stores_zipcode', {
      id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
	  stores_id: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
	  stores_zipcode_id: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
      stores_zipcode: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'stores_zipcode'
    });
  };
  