module.exports = function(sequelize, DataTypes) {
    return sequelize.define('cms', {
      id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      title: {
        type: DataTypes.STRING(255),
        allowNull: false
      },
      slug: {
        type: DataTypes.STRING(255),
        allowNull: false
      }, 
      store_id: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      sequence:{
        type: DataTypes.INTEGER(3).UNSIGNED,
        allowNull: true
      },
      content_heading : {
        type: DataTypes.TEXT,
        allowNull: true
      },
      content_description: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      meta_title: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      meta_key: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      meta_description: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      status: {
        type: DataTypes.ENUM('active','inactive','archive'),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'cms'
    });
  };
  