module.exports = function(sequelize, DataTypes) {
    return sequelize.define('stores', {
      id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
	  store_id: {
        type: DataTypes.STRING(6),
        allowNull: true,
        
      },
      name: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
	  description: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
	  address: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
	  pin: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
	  ph: {
        type: DataTypes.STRING(100),
        allowNull: true
      },
	  persion: {
        type: DataTypes.STRING(200),
        allowNull: true
      },
	   delivery: {
        type: DataTypes.ENUM('yes','no'),
        allowNull: true
      },
      status: {
        type: DataTypes.ENUM('active','inactive','archive'),
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      createdBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedBy: {
        type: DataTypes.STRING(128),
        allowNull: true
      }
    }, {
      tableName: 'stores'
    });
  };
  