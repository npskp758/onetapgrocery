module.exports = function(sequelize, DataTypes) {
    return sequelize.define('attribute_option', {
      id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      attribute_id: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false
      },
      value: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      label: {
        type: DataTypes.STRING(255),
        allowNull: true
      },
      position: {
        type: DataTypes.INTEGER(5).UNSIGNED,
        allowNull: false,
      },
      status: {
        type: DataTypes.INTEGER(6),
        allowNull: false,
        defaultValue: 1
      }
    }, {
      tableName: 'attribute_option'
    });
  };
  