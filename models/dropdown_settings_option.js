/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('dropdown_settings_option', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    dropdown_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    option_value: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    option_label: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    option_order: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'dropdown_settings_option'
  });
};
