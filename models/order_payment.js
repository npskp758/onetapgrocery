module.exports = function(sequelize, DataTypes) {
    return sequelize.define('order_payment', {
      id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      order_id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false
      },
      transaction_id: {
        type: DataTypes.INTEGER(10).UNSIGNED,
        allowNull: false
      },
      method: {
        type: DataTypes.STRING(128),
        allowNull: true
      },
      amount_paid: {
        type: DataTypes.DECIMAL(12,4),
        allowNull: true,
      },
      amount_ordered: {
        type: DataTypes.DECIMAL(12,4),
        allowNull: true,
      },
      amount_cancelled: {
        type: DataTypes.DECIMAL(12,4),
        allowNull: true,
      },
      additional_info: {
        type: DataTypes.TEXT,
        allowNull: true
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    }, {
      tableName: 'order_payment'
    });
  };
  