var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');




exports.dashboardList = function(req, res, next){
	  
    var arrData = null;
    fetch(req.app.locals.apiurl+'dashboard',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
        
    }}) .then(function(response) { return response.json() })
    .then(function(value){
        //return res.send(value);
		return res.render('superpos/home/dashboard', { title: 'Dashboard',arrProduct: value.product,arrOption:'',message:'',errors:''		
		});
	});
}

