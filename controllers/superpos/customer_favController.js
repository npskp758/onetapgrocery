var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');


exports.customer_favList = function(req, res, next){
	  
    var arrData = null;
    fetch(req.app.locals.apiurl+'customer_fav',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
        
    }}) .then(function(response) { return response.json() })
    .then(function(value){
		return res.render('superpos/customer_fav/list', { title: 'Customer Favourite',arrData: value.value,arrStore: value.stores,arrCustomer: value.customer,arrOption:'',message:'',errors:''		
		});
	});
}


exports.addeditCustomer_fav = function(req, res, next){
	var id = req.params.id;  
    var arrData = null;
    var arrStore = null;
	var arrCustomer = null;
    if(!id){
        fetch(req.app.locals.apiurl+'customer_fav/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){

            return res.render('superpos/customer_fav/addedit', {title: 'Customer Favourite ',messages:'',arrData:'',arrStore: value.arrstores,arrCustomer: value.arrcustomer,errors:''});
        });
    }else{
        fetch(req.app.locals.apiurl+'customer_fav/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            return res.render('superpos/customer_fav/addedit', {title: 'Customer Favourite ',messages:'',arrData: value.value,arrStore: value.arrstores,arrCustomer: value.arrcustomer,errors:''});
        });
    }
    
};

exports.deleteCustomer_fav = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'customer_fav/delete/'+id,{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })
		
};



