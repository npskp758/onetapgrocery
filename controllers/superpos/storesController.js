
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');


exports.storesList = function(req, res, next){

var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 10;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingItem = models.stores.findAndCountAll({where: {status: {$ne: 'archive'}},limit: limit, offset: offset});            
            existingItem.then(function (results) {
                const itemCount = results.count;
                const pageCount = Math.ceil(results.count / limit);
                const previousPageLink = paginate.hasNextPages(req)(pageCount);
                const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                console.log(startItemsNumber);
                console.log(endItemsNumber);

                // console.log(previousPageLink)
                return res.render('superpos/stores/list', { title: 'Stores',arrData: results.value,arrData:results.rows,arrOption:'',messages: req.flash('info'),errors:req.flash('errors'),
                    pageCount,
                    itemCount,
                    currentPage: currPage,
                    previousPage : previousPageLink	,
                    startingNumber: startItemsNumber,
                    endingNumber: endItemsNumber,
                    pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                }); 
            })
        }	
    });
}

// exports.addeditStores = function(req, res, next){
    
//     var id = req.params.id;
//     var arrData = null;
//     var arrOption = null;
// 	var arrZipcode =null;
//     if(!id){
//         fetch(req.app.locals.apiurl+'stores/addedit',{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             return res.render('superpos/stores/addedit', {title: 'Add Stores',arrData:'',arrOption:'',arrCity: value.city,Store_city:'',messages: req.flash('info'),arrZipcode: value.arrzipcode,Store_zipcode:'',errors:''});
//         });
//     }else{            
//         fetch(req.app.locals.apiurl+'stores/addedit/'+id,{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             //res.send(value.att_value);	
//             return res.render('superpos/stores/addedit', {title: 'Edit Stores',arrData: value.value,arrOption: value.att_value,arrCity: value.city,Store_city:value.arrStores_city,arrZipcode: value.arrzipcode,Store_zipcode:value.arrStores_zipcode,messages: req.flash('info'),errors:''});

//             });
//     }	
// };


exports.addeditStores = function(req, res, next){
    
    var id = req.params.id;
    var existingItem = null;
    existingCity = models.citi.findAll({ where: {status:'active'} });
    existingCity.then(function (Allcity) {
        
        zipcode = models.zipcode.findAll({ where: {status:'active'} });
        zipcode.then(function (zipcode) {	
            if(!id){	
                //res.status(200).send({ 
                return res.render('superpos/stores/addedit',{
                    title: 'Add Stores',
                    arrData:'',
                    //arrOption:'',
                    arrCity: Allcity,
                    Store_city:'',
                    messages: req.flash('info'),
                    arrZipcode: zipcode,
                    Store_zipcode:'',
                    errors:req.flash('errors')
                    //status:200,
                    //city: Allcity,
                    //arrzipcode:zipcode
                });
            }else{            
                existingItem = models.stores.findOne({ where: {id:id} });
                existingItem.then(function (value) {	
                    existingStores_city = models.stores_city.findAll({ where: {stores_id:id} });
                    existingStores_city.then(function (stores_city) {
                    
                        existingStores_zipcode = models.stores_zipcode.findAll({ where: {stores_id:id} });
                        existingStores_zipcode.then(function (stores_zipcode) {
                        // res.status(200).send({ 
                        return res.render('superpos/stores/addedit',{    
                            title: 'Edit Stores',
                            arrData: value,
                            //arrOption: value.att_value,
                            arrCity: Allcity,
                            Store_city:stores_city,
                            arrZipcode: zipcode,
                            Store_zipcode:stores_zipcode,
                            messages: req.flash('info'),
                            errors:req.flash('errors')
                            //status:200,
                            //value: value,
                            //city: Allcity,
                            //arrStores_city: stores_city,
                            //arrzipcode:zipcode,
                            //arrStores_zipcode:stores_zipcode
                        });
                    })
                })
                });	
            }
        }); 
    }); 
};

exports.addStores = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        var stores_cityArr=fields.stores_city_id ;	
        var stores_zipcodeArr=fields.stores_zipcode_id ;

        var str = fields.name ? fields.name[0] : '';
        //return res.send(typeof str);
        var storesName= str.replace(" ", "").substr(0, 3).toUpperCase();
        //return res.send(storesName);
        var storesID = storesName;
        //return res.send(storesID); 
        

        if(!id){
            var i=0;
            

            models.stores.create({ 
			    name: fields.name?fields.name[0]:null, 
                //store_id: Store_ID,
                description: fields.description?fields.description[0]:null,
				address: fields.address?fields.address[0]:null, 
				//pin: fields.pin?fields.pin:null, 
				ph: fields.ph?fields.ph[0]:null, 
				persion: fields.persion?fields.persion[0]:null, 
				delivery: fields.delivery?fields.delivery[0]:null, 
				//type: fields.type[0]?fields.type[0]:null, 
                status:fields.status?fields.status[0]:null,
                }).then(function(stores) { 
                    var newStores_id= storesID +stores.id;
                    //  return res.send(newStores_id);
                    models.stores.update({ 
                        store_id: newStores_id ? newStores_id : null,                        
                    },{where:{id:stores.id}}).then(function(stores_update) {
                     

                    // store multicity start
                    if(stores_cityArr){                        
                        stores_cityArr.forEach(function(sto_city){
                            sto_city=sto_city.split("~");
                            sto_cityId=sto_city[0];
                            sto_cityName=sto_city[1];
                            models.stores_city.create({
                                stores_id:stores.id,
                                stores_city_id:sto_cityId,
                                stores_city_name:sto_cityName                            
                            });		
                        }, this);
                    }
                    // store multicity end

                    // store multizipcode start
                    if(stores_zipcodeArr){ 
                        stores_zipcodeArr.forEach(function(sto_zipcode){
                            sto_zipcode=sto_zipcode.split("~");
                            sto_zipcodeId=sto_zipcode[0];
                            sto_zip=sto_zipcode[1];
                            models.stores_zipcode.create({
                                stores_id:stores.id,
                                stores_zipcode_id:sto_zipcodeId,
                                stores_zipcode:sto_zip                            	
                            });		
                        }, this);
                    }
                    // store multizipcode end
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/stores');
                }).catch(function(error) {
                    return res.send(error);
                });
            })
            .catch(function(error) {
                
                console.log(error);
                return res.send(error);
            });
        }
        else{
            models.stores.update({ 
			   
				name: fields.name[0]?fields.name[0]:null, 
                description: fields.description[0]?fields.description[0]:null,
				address: fields.address[0]?fields.address[0]:null, 
				//pin: fields.pin?fields.pin[0]:null, 
				ph: fields.ph?fields.ph[0]:null, 
				persion: fields.persion?fields.persion[0]:null, 
				delivery: fields.delivery?fields.delivery[0]:null, 
				//type: fields.type[0]?fields.type[0]:null, 
                status:fields.status?fields.status[0]:null,
            },{where:{id:id}}).then(function(stores) {

                // store multicity start
                models.stores_city.destroy({ where:{stores_id: id}
				});
                if(stores_cityArr){                        
                    stores_cityArr.forEach(function(sto_city){
                        sto_city=sto_city.split("~");
                        sto_cityId=sto_city[0];
                        sto_cityName=sto_city[1];
                        models.stores_city.create({
                            stores_id:id,
                            stores_city_id:sto_cityId,
                            stores_city_name:sto_cityName                            
                        });		
                    }, this);
                }
                // store multicity end

                // store multizipcode start
                models.stores_zipcode.destroy({ where:{stores_id: id}
                });
                if(stores_zipcodeArr){ 
                    stores_zipcodeArr.forEach(function(sto_zipcode){
                        sto_zipcode=sto_zipcode.split("~");
                        sto_zipcodeId=sto_zipcode[0];
                        sto_zip=sto_zipcode[1];
                        models.stores_zipcode.create({
                            stores_id:id,
                            stores_zipcode_id:sto_zipcodeId,
                            stores_zipcode:sto_zip                            	
                        });		
                    }, this);
                }
                req.flash('info','Successfully Updated');
                // res.redirect('back');
                return res.redirect('/superpos/stores');
            })
            .catch(function(error) {
                
                console.log(error);
                return res.send(error);
            });
        }
    });
};

exports.deleteStores = function(req, res, next) {
    var id = req.params.id;
    console.log(id)
    models.stores.destroy({ 
        where:{id:id}
    }).then(function(value) {
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });	
	
};
// exports.deleteStores = function(req, res, next) {
    
//     var id = req.params.id;
//     var token= req.headers['token'];
//     //console.log(token)
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//              res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{    
//             models.stores.destroy({ 
//                 where:{id:id}
//             }).then(function(value) {
//                 //res.status(200).send({ status:200,value:value });
//                 req.flash('message','Successfully Deleted');
//                 res.redirect('back');
//             });
//         }           
//     });		
// };

// exports.deleteStores = function(req, res, next){
// 	var id = req.params.id;	
// 	console.log(id)
//     fetch(req.app.locals.apiurl+'stores/delete/'+id,{headers: {
//       "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
// 	}}).then(function(response) { res.redirect('back'); })	
// }


//exports.deleteCiti = function(req, res, next) {
//     var id = req.params.id;    
//     models.citi.destroy({ 
//         where:{id:id}
//     }).then(function(citi) {
//         models.citi.destroy({ where: {citi_id:id} 
//         }).then(function(att_value){
//         //req.flash('message','Successfully Deleted');
//         res.redirect('back');
//         })
//     });	
// };        