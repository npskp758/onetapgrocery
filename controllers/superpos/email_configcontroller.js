var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');
var config = require('../../config/config.json');
var helpers = require('../../helpers/helper_functions.js');


var Sequelize = require("sequelize");
var sequelize = new Sequelize(
config.development.database, 
config.development.username,
config.development.password, {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
    // SQLite only
    //storage: 'path/to/database.sqlite'
});



//console.log("☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺☺"+helpers.getEmailConfig);

/**
 * Load the email configuration page and displaying the email and phone number
 * var emailConfig_data = await models.email_config.findAll();

    // if(!id){
       if(emailConfig_data.length > 0){ 
 */
exports.email_configadd = async function(req, res, next) {
    // var id=1;

    // if(!id){
       
    //     return res.render('superpos/email_config/addedit', {
    //         title: 'Edit contact',
    //         arrData:'' ,          
    //         messages: req.flash('info'),
    //     });
    // }else{
    //     var emailConfig = await models.email_config.findOne({where:{id:id}});
    //     return res.render('superpos/email_config/addedit', {
    //         title: 'Edit contact',
    //         arrData: emailConfig,          
    //         messages: req.flash('info'),
    //     });
    // }


    var emailConfig_data = await models.email_config.findAll();
    //console.log('1111111111111111111111111111111111');
    //console.log(emailConfig_data[0].email);
    //console.log('1111111111111111111111111111111111');
    if(emailConfig_data){

        return res.render('superpos/email_config/addedit', {
            title: 'Edit contact',
            arrData: emailConfig_data[0],          
            messages: req.flash('info'),
        });

    }else{

        return res.render('superpos/email_config/addedit', {
            title: 'Add contact',
            arrData:'' ,          
            messages: req.flash('info'),
        });

    }
}


exports.add = function(req, res, next) {

    //var logDetails = req.session.userDetails;
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        console.log(id);
        if(id){
                models.email_config.update({ 
                
                email       : fields.email[0],
                phoneno      : fields.phoneno[0],                
               
				
            },{where:{id:id}}).then(function(user) {

                req.flash('info','Successfully Updated');
                return res.redirect('back');
            })
            .catch(function(error) {
                return res.send(error);
            });
        
         } else{
                 models.email_config.create({
                    email: fields.email[0],
                    phoneno: fields.phoneno[0],
                    

                 }).then(function(user) {

                    //return res.send('user created');
                req.flash('message','Successfully Created');	
                res.redirect('back');
                    
        
            })


        }
    });

}
    


