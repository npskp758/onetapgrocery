var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
const Excel = require('exceljs');
var config = require('../../config/config.json');
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
config.development.database, 
config.development.username,
config.development.password, {
    host: 'localhost',
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});


exports.addeditSubcategory = function(req, res, next){
	var id = req.params.id;  
    var arrData = null;
    var arrStore = null;
	var arrCategory = null;
    if(!id){
        fetch(req.app.locals.apiurl+'subcategory/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){

            return res.render('superpos/subcategory/addedit', {title: 'Add Subcategory ',messages:'',arrData:'',arrStore: value.arrstores,arrCategory: value.arrcategory,errors:''});
        });
    }else{
        fetch(req.app.locals.apiurl+'subcategory/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            return res.render('superpos/subcategory/addedit', {title: 'Edit Subcategory ',messages:'',arrData: value.value,arrStore: value.arrstores,arrCategory: value.arrcategory,errors:''});
        });
    }
    
};

exports.subcategoryList =async function(req, res, next){
	  
    var arrData = null;
    // fetch(req.app.locals.apiurl+'subcategory',{headers: {
    //     "Content-Type": "application/json; charset=utf-8",
    //     "token": req.session.token,
        
    // }}) .then(function(response) { return response.json() })
  var  list = await sequelize.query("select b.title 'category', a.title, a.store_id, a.status, a.icon, a.id 'id1' from subcategory as a left join category as b on b.id=a.category order by b.title",{ type: Sequelize.QueryTypes.SELECT });
    // list.then(function(value){
		return res.render('superpos/subcategory/list', { title: 'Sub Category',arrData: list,arrOption:'',message:'',errors:''		
		// });
	});
}

// exports.subcategoryList =async function(req, res, next){
	  
//     var arrData = null;
//     // fetch(req.app.locals.apiurl+'subcategory',{headers: {
//     //     "Content-Type": "application/json; charset=utf-8",
//     //     "token": req.session.token,
        
//     // }}) .then(function(response) { return response.json() })
//   var  list = await sequelize.query("select b.title 'category', a.title, a.store_id, a.status, a.icon, a.id 'id1' from subcategory as a left join category as b on b.id=a.category order by b.title",{ type: Sequelize.QueryTypes.SELECT });
//     // list.then(function(value){
// 		return res.render('superpos/subcategory/list', { title: 'Sub Category',arrData: list,arrOption:'',message:'',errors:''		
// 		// });
// 	});
// }


exports.deleteSubcategory = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'subcategory/delete/'+id,{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })
		
};
exports.downloadSubCategoryList = async function (req, res, next) {
    var workbook = new Excel.Workbook();

    workbook.creator = 'Me';
    workbook.lastModifiedBy = 'Her';
    workbook.created = new Date(1985, 8, 30);
    workbook.modified = new Date();
    workbook.lastPrinted = new Date(2016, 9, 27);
    workbook.properties.date1904 = true;

    workbook.views = [
        {
            x: 0, y: 0, width: 10000, height: 20000,
            firstSheet: 0, activeTab: 1, visibility: 'visible'
        }
    ];
    var worksheet = workbook.addWorksheet('My Sheet');
    worksheet.columns = [
        { header: 'Sl. No.', key: 'Slno', width: 10 },
        { header: 'Category Name ', key: 'Catagoryname', width: 30 },
        { header: 'Sub Category Name ', key: 'Subcatagoryname', width: 25 },
        { header: 'Number Of Product ', key: 'product', width: 25 }
       
           ];

    var list = await sequelize.query("select b.title 'category_title', a.title 'sub_category_title', (select count(*) from product where sub_category_id =a.id) 'total_products' from subcategory as a left join category as b on b.id=a.category order by b.title",{ type: Sequelize.QueryTypes.SELECT });
    
   var prv_catg_title = '';
   for (var i = 0; i < list.length; i++) {

        worksheet.addRow({ Slno: i+1, Catagoryname: (list[i].category_title != prv_catg_title ? list[i].category_title : ''), Subcatagoryname:list[i].sub_category_title, product:list[i].total_products  });
        prv_catg_title = list[i].category_title;
    }

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader("Content-Disposition", "attachment; filename=" + "Subcategory-list.xlsx");
    workbook.xlsx.write(res)
        .then(function (data) {
            res.end();
            console.log('File write done........');
        });
};


