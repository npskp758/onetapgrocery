var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');



exports.coupon_transactionList = function(req, res, next){


var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 10;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingItem = models.coupon_transaction.findAndCountAll({where: {status:'active'},limit: limit, offset: offset});            
            existingItem.then(function (results) {
                existingItem = models.customer.findAll();
                existingItem.then(function (customer) {
                    const itemCount = results.count;
                    const pageCount = Math.ceil(results.count / limit);
                    const previousPageLink = paginate.hasNextPages(req)(pageCount);
                    const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                    const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                    console.log(startItemsNumber);
                    console.log(endItemsNumber);

                    // console.log(previousPageLink)
                        return res.render('superpos/coupon_transaction/list', { title: 'Coupon Transaction',arrData:results.rows,arrCustomer: customer,messages:req.flash('info'),errors:req.flash('errors'),		
                        pageCount,
                        itemCount,
                        currentPage: currPage,
                        previousPage : previousPageLink	,
                        startingNumber: startItemsNumber,
                        endingNumber: endItemsNumber,
                        pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                    }); 
                })
            })    
        }	
    });
}

// exports.addeditCoupon_transaction = function(req, res, next){
    
//     var id = req.params.id;
//     var existingItem = null;
    
//     if(!id){	
//         //res.status(200).send({ 
//         return res.render('superpos/coupon/addedit', {
//             title: 'Add Coupon',
//             messages:req.flash('info'),
//             arrData:'',
//             errors:req.flash('errors'),
//         });
//     }else{            
//         existingItem = models.coupon.findOne({ where: {id:id} });
//         existingItem.then(function (value) {                    
//             //res.status(200).send({ 
//             return res.render('superpos/coupon/addedit', {
//                 title: 'Edit Coupon',
//                 messages:req.flash('info'),
//                 arrData: value,
//                 errors:req.flash('errors'),
//             });
            
//         });
//     }
// };


// exports.addCoupon_transaction = function(req, res, next) {

//     var d = new Date();
//     var n = d.getTime();
//     var form = new multiparty.Form();
//     var logdetails = req.session.user 
//     form.parse(req, function(err, fields, files) { 
//         var id = fields.update_id[0];
//         if(!id)
//         {
//             models.coupon.create({
//                 coupon_type: fields.coupon_type ? fields.coupon_type[0] : null,
//                 coupon_value: fields.coupon_value ? fields.coupon_value[0] : 0,
//                 date_from: fields.date_from ? fields.date_from[0] : null,
//                 date_to: fields.date_to ? fields.date_to[0] : null,
//                 time_from: fields.time_from ? fields.time_from[0] : null, 
//                 time_to: fields.time_to ? fields.time_to[0] : null,
//                 coupon_code: fields.coupon_code ? fields.coupon_code[0] : null,
//                 purchase_limit: fields.purchase_limit ? fields.purchase_limit[0] : null,
//                 status: fields.status ? fields.status[0] : null,
//                 createdBy : logdetails ? logdetails.id : '' 
//             }).then(function(coupon) {
//                 req.flash('info','Successfully Created');	  
//                 res.redirect('/superpos/coupon');
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }else{
//             models.coupon.update({ 
//                 coupon_type: fields.coupon_type ? fields.coupon_type[0] : null,
//                 coupon_value: fields.coupon_value ? fields.coupon_value[0] : 0,
//                 date_from: fields.date_from ? fields.date_from[0] : null,
//                 date_to: fields.date_to ? fields.date_to[0] : null,
//                 time_from: fields.time_from ? fields.time_from[0] : null, 
//                 time_to: fields.time_to ? fields.time_to[0] : null,
//                 coupon_code: fields.coupon_code ? fields.coupon_code[0] : null,
//                 purchase_limit: fields.purchase_limit ? fields.purchase_limit[0] : null,
//                 status: fields.status ? fields.status[0] : null,
//                 updatedBy : logdetails ? logdetails.id : '' 
//             },{where:{id:id}}).then(function(coupon) {
//                 req.flash('info','Successfully Updated');	  
//                 res.redirect('/superpos/coupon');      
//             })
//             .catch(function(error) {
//                 return res.send(error);
//             });
//         }
//     });
// };

exports.deleteCoupon_transaction = function(req, res, next) {    
    var id = req.params.id;
       
    models.coupon_transaction.update({ 
        status: 'archive',
    },{where:{id:id}
    }).then(function(value) {
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });	
};







