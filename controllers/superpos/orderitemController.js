var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');



exports.orderitemList = function(req, res, next){
	  
//     var arrData = null;
//     fetch(req.app.locals.apiurl+'order',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
        
//     }}) .then(function(response) { return response.json() })
//     .then(function(data){
// 		return res.render('superpos/orderitem/list', { title: 'Order Item',arrData:data,arrOption:'',message:'',errors:''		
// 		});
// 	});
// }

var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 10;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingItem = models.	order_item.findAndCountAll({limit: limit, offset: offset});            
            existingItem.then(function (results) {
                const itemCount = results.count;
                const pageCount = Math.ceil(results.count / limit);
                const previousPageLink = paginate.hasNextPages(req)(pageCount);
                const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                console.log(startItemsNumber);
                console.log(endItemsNumber);

                // console.log(previousPageLink)
               
				return res.render('superpos/orderitem/list', { title: 'Order Item',arrData:results,arrOption:'',message:'',arrData:results.rows,errors:'',		
				pageCount,
                    itemCount,
                    currentPage: currPage,
                    previousPage : previousPageLink	,
                    startingNumber: startItemsNumber,
                    endingNumber: endItemsNumber,
                    pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                }); 
            })
        }	
	});
}



exports.addeditOrderitem = function(req, res, next){

	var id = req.params.id;  
	var arrData = null;
	var arrCategory = null;
		if(!id){
			fetch(req.app.locals.apiurl+'orderitem/addedit',{headers: {
				"Content-Type": "application/json; charset=utf-8",
				"token": req.session.token,
			}}) .then(function(response) { return response.json() })
				.then(function(value){
					
			res.render('superpos/orderitem/addedit',{title:'Add Oder Item',arrData:'',arrOrder:value.order,arrStore:value.store,arrProduct:value.product,messages: req.flash('info'),errors: req.flash('errors')});
				});
		}else{
			fetch(req.app.locals.apiurl+'orderitem/addedit/'+id,{headers: {
				"Content-Type": "application/json; charset=utf-8",
				"token": req.session.token,
			}}) .then(function(response) { return response.json() })
				.then(function(value){
					
					res.render('superpos/orderitem/addedit',{title:'Edit Oder Item',arrData: value.value,arrOrder:value.order,arrStore:value.store,arrProduct:value.product,messages: req.flash('info'),errors: req.flash('errors')});
				});
		}
};







