
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');



exports.citiList = function(req, res, next){
	  
//     var arrData = null;
	
//     fetch(req.app.locals.apiurl+'citi',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
//     }}) .then(function(response) { return response.json() })
//     .then(function(value){
// 		return res.render('superpos/citi/list', { title: 'City',arrData: value.value,arrOption:'',message: req.flash('message'),errors:''		
// 		});
// 	});
// }

var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 10;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingItem = models.citi.findAndCountAll({limit: limit, offset: offset});            
            existingItem.then(function (results) {
                const itemCount = results.count;
                const pageCount = Math.ceil(results.count / limit);
                const previousPageLink = paginate.hasNextPages(req)(pageCount);
                const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                console.log(startItemsNumber);
                console.log(endItemsNumber);

                // console.log(previousPageLink)
                return res.render('superpos/citi/list', { title: 'City',arrData: results.value,arrOption:'',arrData:results.rows,messages: req.flash('info'),errors:'',		
                    pageCount,
                    itemCount,
                    currentPage: currPage,
                    previousPage : previousPageLink	,
                    startingNumber: startItemsNumber,
                    endingNumber: endItemsNumber,
                    pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                }); 
            })
        }	
    });
}
    




// exports.addeditCiti = function(req, res, next){
    
//     var id = req.params.id;
//     var arrData = null;
//     var arrOption = null;
// 	var arrStore = null;
//     if(!id){
//         fetch(req.app.locals.apiurl+'citi/addedit',{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             return res.render('superpos/citi/addedit', {title: 'Add City',arrData:'',messages: req.flash('info'),errors:''});
//         });
//     }else{            
//         fetch(req.app.locals.apiurl+'citi/addedit/'+id,{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//          //  return res.send(value.value);
//             return res.render('superpos/citi/addedit', {title: 'Edit City',arrData: value.value,messages: req.flash('info'),errors:''});

//             });
//     }	
// };

exports.addeditCiti = function(req, res, next){
    
    var id = req.params.id;
    var existingItem = null;
    if(!id){	
        //res.status(200).send({ 
        return res.render('superpos/citi/addedit', {
            title: 'Add City',
            arrData:'',
           // arrOption:'',
            messages: req.flash('info'),
            //arrStore: value.arrData,
            errors:'',
            //status:200
        });
    }else{            
        existingItem = models.citi.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
            //    res.status(200).send({ 
            return res.render('superpos/citi/addedit', {
                title: 'Edit City',
                arrData: value,
                //arrOption: value.att_value,
                messages: req.flash('info'),
                //arrStore: value.arrData,
                errors:'',
                //status:200,
                //value: value
            });
        });	
    }	
};

exports.addCiti = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        
        if(!id){
            models.citi.create({ 
                name: fields.name[0]?fields.name[0]:null, 
                //store_id: fields.store_id?fields.store_id[0]:null,
                status:fields.status[0]?fields.status[0]:null,
                }).then(function(citi) {  
                     
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/citi');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.citi.update({ 
                name: fields.name[0]?fields.name[0]:null, 
                //store_id: fields.store_id[0]?fields.store_id[0]:null,
                
                status:fields.status[0]?fields.status[0]:null,
            },{where:{id:id}}).then(function(citi) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/citi');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};

// exports.deleteCiti = function(req, res, next){
// 	var id = req.params.id;	
// 	console.log(id)
//     fetch(req.app.locals.apiurl+'citi/delete/'+id,{headers: {
//       "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
// 	}}).then(function(response) { res.redirect('back'); })	
// }

exports.deleteCiti = function(req, res, next) {
    
    var id = req.params.id;
    models.citi.destroy({ 
        where:{id:id}
    }).then(function(value) {
        //res.status(200).send({ status:200,value:value });
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });		
};   