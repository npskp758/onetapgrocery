var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');


exports.settings_profileList = function(req, res, next){
	  
    var arrData = null;
    fetch(req.app.locals.apiurl+'settings_profile',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
        
    }}) .then(function(response) { return response.json() })
    .then(function(value){
		return res.render('superpos/settings_profile/List', { title: 'Setting Profile',arrData: value.value,arrOption:'',message:'',errors:''		
		});
	});
}
exports.addeditSettings_profile = function(req, res, next){
    //return res.send(req.body.data.email);
    var id = req.params.id;
    var arrData = null;
    var arrOption = null;
    if(!id){
        fetch(req.app.locals.apiurl+'settings_profile/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            return res.render('superpos/settings_profile/addedit', {title: 'Add Profile',arrData:'',arrOption:'',messages: req.flash('info'),errors:''});
        });
    }else{            
        fetch(req.app.locals.apiurl+'settings_profile/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
          // return res.send(value.value);
            return res.render('superpos/settings_profile/addedit', {title: 'Edit Profile',arrData: value.value,arrOption: value.att_value,messages: req.flash('info'),errors:''});

            });
    }	
};

exports.deleteSettings_profile = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'settings_profile/delete/'+id,{headers: {
      "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })	
}

