
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');


exports.notificationList = function(req, res, next){
	  
    var arrData = null;
    fetch(req.app.locals.apiurl+'notification',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
    }}) .then(function(response) { return response.json() })
    .then(function(value){
		return res.render('superpos/notification/list', { title: 'Notification',arrData: value.value,arrOption:'',message: req.flash('message'),errors:''		
		});
	});
}

exports.addeditNotification = function(req, res, next){
    
    var id = req.params.id;
    var arrData = null;
    var arrOption = null;
   
    if(!id){
        fetch(req.app.locals.apiurl+'notification/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            return res.render('superpos/notification/addedit', {title: 'Add Notification',arrData:'',arrOption:'',messages: req.flash('info'),errors:''});
        });
    }else{            
        fetch(req.app.locals.apiurl+'notification/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            //res.send(value.att_value);	
            return res.render('superpos/notification/addedit', {title: 'Edit Notification',arrData: value.value,arrOption: value.att_value,messages: req.flash('info'),errors:''});

            });
    }	
};

exports.deleteNotification = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'notification/delete/'+id,{headers: {
      "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })	
}

  
