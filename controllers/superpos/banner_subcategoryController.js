var models= require('../../models');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var fetch = require('node-fetch');
var helper = require('../../helpers/helper_functions');

exports.subcategoryBannerList=function(req,res,next){


    banner_list= models.banner_subcategory.findAll({order: [['id', 'DESC']] });
    banner_list.then(function (value){
        console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"+value);
    return res.render('superpos/banner_subcategory/list',
    { 
        title: 'Banner Display',
        arrData: value,
        helper: helper
    });
});
}

exports.subcategoryBannerAddedit= function(req,res,next){
    var id = req.params.id;
    if(!id){
      return res.render('superpos/banner_subcategory/addedit',
      { 
          title: 'Banner Upload',
          arrData:'',
          helper: helper
        });  
    
}else{
    existingItem = models.banner_subcategory.findOne({ where: {id:id} });
    existingItem.then(function (value) {	
    return res.render('superpos/banner_subcategory/addedit',
    { 
        title: 'Banner Upload',
        arrData:value,
        helper: helper
      });
    });  


}
};
    exports.add = function(req, res, next) {

        var form = new multiparty.Form();
        form.parse(req, function(err, fields, files) { 
            // return res.send(fields);
            var id = fields.update_id[0];
  
            if(!id){
                
                models.banner_subcategory.create({ 
                    
                    status      : fields.status[0],
                    image : files.image[0].originalFilename,
                    
                    }).then(function(banner) { 
                        
                        helper.createDirectory('public/superpos/myimages/subcategory-banner/');

                        if(files.image[0].originalFilename != '') {
                            var temp_path = files.image[0].path;
                            var file_name = files.image[0].originalFilename;
                            var target_path = file_name;
                            helper.uploadFile(temp_path, target_path);
                        }
                            
                        req.flash('info','Successfully created!');
                        return res.redirect('back');
                    })
                    .catch(function(error) {
                        return res.send(error);
                    });
            }
            else{
              var  image1 = files.image[0].originalFilename;
               // console.log("----------------------------------------++++++++++++++++++........................................."+image1);
                if(image1 ==''){
                   // console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                    models.banner_subcategory.update({ 
                        status      : fields.status[0],
                    },{where:{id:id}}).then(function(banner)  { 
                            
                        helper.createDirectory('public/superpos/myimages/subcategory-banner/');
    
                        
    
                        req.flash('info','Successfully Updated');
                        return res.redirect('back');
                    })
                
                    .catch(function(error) {
                        return res.send(error);
                    });
                

                }else{
                  //  console.log("------------------------------------------------------------------------------------")
                models.banner_subcategory.update({ 
                    status      : fields.status[0],
                    image : files.image[0].originalFilename,
                },{where:{id:id}}).then(function(banner)  { 
                        
                    helper.createDirectory('public/superpos/myimages/subcategory-banner/');

                    if(files.image[0].originalFilename != '') {
                        var temp_path = files.image[0].path;
                        var file_name = files.image[0].originalFilename;
                        var target_path = file_name;
                        helper.uploadFile(temp_path, target_path);
                    }

                    req.flash('info','Successfully Updated');
                    return res.redirect('back');
                })
            
                .catch(function(error) {
                    return res.send(error);
                });
            }

                
            }
        });
    
    }
    exports.delete = function(req, res, next) {
    
    
        var id = req.params.id;
        models.banner_subcategory.destroy({ 
            where:{id:id}
        }).then(function(value) {
            //res.status(200).send({ status:200,value:value });
            req.flash('info','Successfully Deleted');
            res.redirect('back');
            
        });		
    }; 



