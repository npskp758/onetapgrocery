var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');



exports.addeditAccount_setting = function(req, res, next){ 
    var arrStore = null;
    var arrCity= null;
    var arrZip= null;
    var logdetails = req.session.user;
    fetch(req.app.locals.apiurl+'account_setting/addedit/'+logdetails.id,{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
    }}) .then(function(response) { return response.json() })
        .then(function(value){
        // console.log('value');
        // res.send(value.value);
        res.render('superpos/account_setting/addedit',{title: 'Account Settings',arrData:value.value,arrcit:value.value,arrpin:value.value,messages: req.flash('info'),arrStore: value.arrData,arrCity:value.arrcit,arrZip:value.arrpin,errors: req.flash('errors')});
    
    });
};




exports.fileupload = function (req,res) {
    	var formnew = new formidable.IncomingForm();
    	formnew.parse(req);
    	formnew.on('fileBegin', function (name, file) {
    		//console.log(file.name);
    		if (file.name && file.name != '') {
    			file.path = __dirname + '/../../public/superpos/myimages/' + file.name;
    		}
    	});	
    };
    