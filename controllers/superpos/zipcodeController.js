
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');


exports.zipcodeList = function(req, res, next){
	  
//     var arrData = null;
//     fetch(req.app.locals.apiurl+'zipcode',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
//     }}) .then(function(response) { return response.json() })
//     .then(function(value){
// 		return res.render('superpos/zipcode/list', { title: 'Zipcode',arrData: value.value,arrOption:'',message: req.flash('message'),errors:''		
// 		});
// 	});
// }

var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 10;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
jwt.verify(token, SECRET, function(err, decoded) {
    if (err) {
        res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
    }else{
        existingItem = models.zipcode.findAndCountAll({limit: limit, offset: offset});            
        existingItem.then(function (results) {
            const itemCount = results.count;
            const pageCount = Math.ceil(results.count / limit);
            const previousPageLink = paginate.hasNextPages(req)(pageCount);
            const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
            const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
            console.log(startItemsNumber);
            console.log(endItemsNumber);

            // console.log(previousPageLink)
                return res.render('superpos/zipcode/list', { title: 'Zipcode',arrData: results.value,arrOption:'',arrData:results.rows,messages: req.flash('info'),errors:req.flash('errors'),	
                pageCount,
                itemCount,
                currentPage: currPage,
                previousPage : previousPageLink	,
                startingNumber: startItemsNumber,
                endingNumber: endItemsNumber,
                pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
            }); 
        })
    }	
});
}


// exports.addeditZipcode = function(req, res, next){
// 	var id = req.params.id;  
//     var arrData = null;
    
//     if(!id){
//         fetch(req.app.locals.apiurl+'zipcode/addedit',{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){

//             return res.render('superpos/zipcode/addedit', {title: 'Add zipcode ',messages:'',arrData:'',errors:''});
//         });
//     }else{
//         fetch(req.app.locals.apiurl+'zipcode/addedit/'+id,{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             return res.render('superpos/zipcode/addedit', {title: 'Edit zipcode ',messages:'',arrData: value.value,errors:''});
//         });
//     }
    
// };

exports.addeditZipcode = function(req, res, next){    
    var id = req.params.id;
    var existingItem = null;
    if(!id){	
        //res.status(200).send({
        return res.render('superpos/zipcode/addedit', {
            title: 'Add zipcode ',
            messages:req.flash('info'),
            arrData:'',
            errors:'',
            //status:200
        });
    }else{            
        existingItem = models.zipcode.findOne({ where: {id:id} });
        existingItem.then(function (value) {	
            //res.status(200).send({
            return res.render('superpos/zipcode/addedit', {
                title: 'Edit zipcode ',
                messages:req.flash('info'),
                arrData: value,
                errors:'',
                //status:200,
                //value: value
            });
        });	
    }	
};

exports.addZipcode = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        var zipcodeArr=fields.optionValue;	
        if(!id){
            models.zipcode.create({ 
                zipcode: fields.zipcode[0]?fields.zipcode[0]:null, 
                // store_id: fields.store_id[0]?fields.store_id[0]:null,
                status:fields.status[0]?fields.status[0]:null,
                }).then(function(zipcode) {  
                    
                    req.flash('info','Successfully Created');
                    return res.redirect('/superpos/zipcode');
                })
                .catch(function(error) {
                    return res.send(error);
                });
        }
        else{
            models.zipcode.update({ 
                zipcode: fields.zipcode[0]?fields.zipcode[0]:null, 
                // store_id: fields.store_id[0]?fields.store_id[0]:null,
                
                status:fields.status[0]?fields.status[0]:null,
            },{where:{id:id}}).then(function(citi) {
                req.flash('info','Successfully Updated');
                return res.redirect('/superpos/zipcode');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};

// exports.deleteZipcode = function(req, res, next){
// 	var id = req.params.id;	
// 	console.log(id)
//     fetch(req.app.locals.apiurl+'zipcode/delete/'+id,{headers: {
//       "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
// 	}}).then(function(response) { res.redirect('back'); })	
// }

exports.deleteZipcode = function(req, res, next) {
    
    var id = req.params.id;
      
    models.zipcode.destroy({ 
        where:{id:id}
    }).then(function(value) {
        //res.status(200).send({ status:200,value:value });
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });
};