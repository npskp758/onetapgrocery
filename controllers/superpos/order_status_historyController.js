var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');



exports.order_status_historyList = function(req, res, next){
	  
//     var arrData = null;
//     fetch(req.app.locals.apiurl+'order_status_history',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
        
//     }}) .then(function(response) { return response.json() })
//     .then(function(value){
// 		return res.render('superpos/order_status_history/list', { title: 'Order Status',arrData: value.value,arrOption:'',message:'',errors:''		
// 		});
// 	});
// }

var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 10;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
jwt.verify(token, SECRET, function(err, decoded) {
    if (err) {
        res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
    }else{
        existingItem = models.order_status_history.findAndCountAll({limit: limit, offset: offset});            
        existingItem.then(function (results) {
            const itemCount = results.count;
            const pageCount = Math.ceil(results.count / limit);
            const previousPageLink = paginate.hasNextPages(req)(pageCount);
            const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
            const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
            console.log(startItemsNumber);
            console.log(endItemsNumber);

            // console.log(previousPageLink)
                return res.render('superpos/order_status_history/list', { title: 'Order Status',arrData: results.value,arrOption:'',message:'',arrData:results.rows,errors:'',		
                pageCount,
                itemCount,
                currentPage: currPage,
                previousPage : previousPageLink	,
                startingNumber: startItemsNumber,
                endingNumber: endItemsNumber,
                pages: paginate.getArrayPages(req)(5, pageCount, currPage)	
            }); 
        })
    }	
});
}



exports.addeditOrder_status_history = function(req, res, next){
    
    var id = req.params.id;
    var arrData = null;
    var arrOption = null;
	var arrorder = null;
    if(!id){
        fetch(req.app.locals.apiurl+'order_status_history/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            return res.render('superpos/order_status_history/addedit', {title: 'Order Status History',arrData:'',arrOption:'',messages: req.flash('info'),arrorder: value.arrData,errors:''});
        });
    }else{            
        fetch(req.app.locals.apiurl+'order_status_history/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
         //  return res.send(value.value);
            return res.render('superpos/order_status_history/addedit', {title: 'Order Status History',arrData: value.value,arrOption: value.att_value,messages: req.flash('info'),arrorder: value.arrData,errors:''});

            });
    }	
};

exports.deleteOrder_status_history = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'order_status_history/delete/'+id,{headers: {
      "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })	
}





