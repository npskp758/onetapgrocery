var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');



exports.shipping_methodList = function(req, res, next){
	  
//     var arrData = null;
//     fetch(req.app.locals.apiurl+'shipping_method',{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
        
//     }}) .then(function(response) { return response.json() })
//     .then(function(data){
// 		return res.render('superpos/shipping_method/list', { title: 'Shipping Method',arrData:data.value,arrCustomer:data.customer,arrOption:'',message:'',errors:''		
// 		});
// 	});
// }


var currPage = req.query.page ? req.query.page : 0;
var limit = req.query.limit ? req.query.limit : 5;
var offset = currPage!=0 ? (currPage * limit) - limit : 0;
var token= req.session.token;
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            existingItem = models.shipping_method.findAndCountAll({where: {status:'active'},limit: limit, offset: offset});            
            existingItem.then(function (results) {
                const itemCount = results.count;
                const pageCount = Math.ceil(results.count / limit);
                const previousPageLink = paginate.hasNextPages(req)(pageCount);
                const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                console.log(startItemsNumber);
                console.log(endItemsNumber);

                // console.log(previousPageLink)
                    return res.render('superpos/shipping_method/list', { title: 'Shipping Method',arrData:results.value,arrCustomer:results.customer,arrOption:'',arrData:results.rows,messages:req.flash('info'),errors:req.flash('errors'),		
                    pageCount,
                    itemCount,
                    currentPage: currPage,
                    previousPage : previousPageLink	,
                    startingNumber: startItemsNumber,
                    endingNumber: endItemsNumber,
                    pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                }); 
            })
        }	
    });
}

// exports.addeditShipping_method = function(req, res, next){
// 	var id = req.params.id;  
//     var arrData = null;
//     var arrProduct = null;
//     if(!id){
//         fetch(req.app.locals.apiurl+'shipping_method/addedit',{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){

//             return res.render('superpos/shipping_method/addedit', {title: 'Add Shipping Method',messages:req.flash('info'),arrData:'',arrcustomer: value.customer,errors:''});
//         });
//     }else{
//         fetch(req.app.locals.apiurl+'shipping_method/addedit/'+id,{headers: {
//             "Content-Type": "application/json; charset=utf-8",
//             "token": req.session.token,
//         }}) .then(function(response) { return response.json() })
//         .then(function(value){
//             return res.render('superpos/shipping_method/addedit', {title: 'Edit Shipping Method',messages:req.flash('info'),arrData: value.value,arrcustomer: value.customer,errors:''});
//         });
//     }
    
// };

exports.addeditShipping_method = function(req, res, next){
    
    var id = req.params.id;
    var existingItem = null;
    if(!id){	
        //res.status(200).send({ 
        return res.render('superpos/shipping_method/addedit', {
            title: 'Add Shipping Method',
            messages:req.flash('info'),
            arrData:'',
            //arrcustomer: value.customer,
            errors:req.flash('errors'),
            //status:200
        });
    }else{            
        existingItem = models.shipping_method.findOne({ where: {id:id} });
        existingItem.then(function (value) {                    
            // res.status(200).send({ 
            return res.render('superpos/shipping_method/addedit', {
                title: 'Edit Shipping Method',
                messages:req.flash('info'),
                arrData: value,
                //arrcustomer: value.customer,
                errors:req.flash('errors'),
                //status:200,
                //value: value
            });
            
        });
    }
};

exports.addShipping_method = function(req, res, next) {

    var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    var logdetails = req.session.user 
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        if(!id)
        {
            var slug=slugify(fields.name[0]);
            models.shipping_method.create({ 
                name: fields.name ? fields.name[0] : null,
                slug: slug,
                price: fields.price ? fields.price[0] : null,
                from_time: fields.from_time ? fields.from_time[0] : null,
                to_time: fields.to_time ? fields.to_time[0] : null,
                status: fields.status ? fields.status[0] : null,
                createdBy : logdetails ? logdetails.id : '' 
            }).then(function(shipping_method) {
                req.flash('info','Successfully Created');	  
                res.redirect('/superpos/shipping_method');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }else{
            models.shipping_method.update({ 
                name: fields.name ? fields.name[0] : null,
                price: fields.price ? fields.price[0] : null,
                from_time: fields.from_time ? fields.from_time[0] : null,
                to_time: fields.to_time ? fields.to_time[0] : null,
                status: fields.status ? fields.status[0] : null,
                updatedBy : logdetails ? logdetails.id : '' 
            },{where:{id:id}}).then(function(shipping_method) {
                req.flash('info','Successfully Updated');	  
                res.redirect('/superpos/shipping_method');      
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
};

// exports.deleteShipping_method = function(req, res, next){
// 	var id = req.params.id;	
// 	console.log(id)
//     fetch(req.app.locals.apiurl+'shipping_method/delete/'+id,{headers: {
//         "Content-Type": "application/json; charset=utf-8",
//         "token": req.session.token,
// 	}}).then(function(response) { res.redirect('back'); })
		
// };


exports.deleteShipping_method = function(req, res, next) {    
    var id = req.params.id;
        
    models.shipping_method.update({ 
        status: 'archive',
    },{where:{id:id}
    }).then(function(value) {
        //res.status(200).send({ status:200,value:value });
        req.flash('info','Successfully Deleted');
        res.redirect('back');
    });
};






