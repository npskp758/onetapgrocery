
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');





exports.add = function(req, res, next){	
		//var data = req.body;
		
	var d = new Date();
    var n = d.getTime();
    var form = new multiparty.Form();
    var logdetails = req.session.user 
    //return res.send(logdetails);
    form.parse(req, function(err, fields, files) { 
        var id = fields.update_id[0];
        if(!id)
        {
            //var slug=slugify(fields.name[0]);
            models.site_settings.create({ 
                site_name: fields.site_name ? fields.site_name[0] : null,
                email: fields.email ? fields.email[0] : null,
                mobile_no: fields.mobile_no ? fields.mobile_no[0] : null,
                app_version: fields.app_version ? fields.app_version[0] : null,
                fax: fields.fax ? fields.fax[0] : null,
                site_url: fields.site_url ? fields.site_url[0] : null,
                address: fields.address ? fields.address[0] : null,
                feature: fields.feature ? fields.feature[0] : null,
                max_pro_availability: fields.max_pro_availability ? fields.max_pro_availability[0] : null,
                free_shipping_limit: fields.free_shipping_limit ? fields.free_shipping_limit[0] : null,
                shipping_charges: fields.shipping_charges ? fields.shipping_charges[0] : null,
                createdBy : logdetails ? logdetails.id : '' 
            }).then(function(site_settings) {
                req.flash('info','Successfully Created');	  
                res.redirect('/superpos/sitesettings');
            })
            .catch(function(error) {
                return res.send(error);
            });
        }else{
            models.site_settings.update({ 
				site_name: fields.site_name ? fields.site_name[0] : null,
                email: fields.email ? fields.email[0] : null,
                mobile_no: fields.mobile_no ? fields.mobile_no[0] : null,
                app_version: fields.app_version ? fields.app_version[0] : null,
                fax: fields.fax ? fields.fax[0] : null,
                site_url: fields.site_url ? fields.site_url[0] : null,
                address: fields.address ? fields.address[0] : null,
                feature: fields.feature ? fields.feature[0] : null,
                max_pro_availability: fields.max_pro_availability ? fields.max_pro_availability[0] : null,
                free_shipping_limit: fields.free_shipping_limit ? fields.free_shipping_limit[0] : null,
                shipping_charges: fields.shipping_charges ? fields.shipping_charges[0] : null,
                updatedBy : logdetails ? logdetails.id : '' 
            },{where:{id:id}}).then(function(site_settings) {
                req.flash('info','Successfully Updated');	  
                res.redirect('/superpos/sitesettings');      
            })
            .catch(function(error) {
                return res.send(error);
            });
        }
    });
	

}
	



exports.addedit = function(req, res, next) {
	var existingItem = null;
	existingItem = models.site_settings.findAll();
	existingItem.then(function (site_settings) {
		if( site_settings.length === 0){
            res.render('superpos/sitesettings/addedit',{
                title:'Site Settings',
                arrData:'',
                messages: req.flash('info'),
                errors: req.flash('errors')
            });
		}else{
			res.render('superpos/sitesettings/addedit',{
                title:'Site Settings',
                arrData:site_settings[0],
                messages: req.flash('info'),
                errors: req.flash('errors')
            });
			
		}
	});	
};
