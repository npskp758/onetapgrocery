var helpers = require('../../helpers/helper_functions.js');
var models = require('../../models');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');
var jwt = require('jsonwebtoken');
var SECRET = 'nodescratch';
const paginate = require('express-paginate');
var flash = require('connect-flash');
var config = require('../../config/config.json');
var Sequelize = require("sequelize");
var sequelize = new Sequelize(
config.development.database, 
config.development.username,
config.development.password, {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
    max: 5,
    min: 0,
    idle: 10000
    },
    // SQLite only
    //storage: 'path/to/database.sqlite'
});
var config = require('../../config/config.json');
// For Mail Send Through MailGun
const emailConfig = require('../../config/email-config')();
const mailgun = require('mailgun-js')(emailConfig);
var sms_controller = require('../sms/smsController');


exports.orderList = function(req, res, next){

    var token= req.session.token;
    var currPage = req.query.page ? req.query.page : 0;
    var limit = req.query.limit ? req.query.limit : 10;
    var offset = currPage!=0 ? (currPage * limit) - limit : 0;
    var keyword = req.query.search ? req.query.search.trim() : '';
    jwt.verify(token, SECRET, function(err, decoded) {
        if (err) {
            res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
        }else{
            sequelize.query("SELECT COUNT(*) AS orderCount FROM `order` where customer_name like  '%"+keyword+"%' or customer_mobile like  '%"+keyword+"%' or order_id like  '%"+keyword+"%' or shipping_address like  '%"+keyword+"%' or grand_total like  '%"+keyword+"%' ",{ type: Sequelize.QueryTypes.SELECT })
            .then(function (ordercount) {
                 console.log("///////////////////"+ordercount[0].orderCount)
                if(ordercount ){
                    //sequelize.query("SELECT `order`.*, (SELECT COUNT(*) FROM order_item WHERE order_item.order_id = order.id) as `numberOfOrder`, `dropdown_settings_option`.`option_label` as `orderStatusTitle` FROM `order` LEFT JOIN `dropdown_settings_option` ON `order`.`order_status`= `dropdown_settings_option`.`option_value` ORDER BY `id` DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT })
                    sequelize.query("SELECT `order`.*, (SELECT COUNT(*) FROM order_item WHERE order_item.order_id = order.id) as `numberOfOrder`, `dropdown_settings_option`.`option_label` as `orderStatusTitle`, `shipping_method`.`price` as `ShippingPrice`, `customer`.`first_name` as `customer_first_name`, `customer`.`last_name` as `customer_last_name` FROM `order` LEFT JOIN `customer` ON `order`.`customer_id`= `customer`.`id` LEFT JOIN `shipping_method`  ON `order`.`shipping_method`= `shipping_method`.`name` LEFT JOIN `dropdown_settings_option` ON `order`.`order_status`= `dropdown_settings_option`.`option_value`  where order.customer_name like  '%"+keyword+"%' or order.customer_mobile like  '%"+keyword+"%' or order.order_id like  '%"+keyword+"%' or order.shipping_address like  '%"+keyword+"%' or order.grand_total like  '%"+keyword+"%'  ORDER BY `order`.`id` DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT })
                   // sequelize.query("SELECT `order`.*, (SELECT COUNT(*) FROM order_item WHERE order_item.order_id = order.id) as `numberOfOrder`, `order_item`.`name` as `orderItemName`,`order_item`.`qty` as `orderItemqty`,`order_item`.`original_price` as `orderItemoriginal_price`, `dropdown_settings_option`.`option_label` as `orderStatusTitle`, `shipping_method`.`price` as `ShippingPrice` FROM `order` LEFT JOIN `shipping_method`  ON `order`.`shipping_method`= `shipping_method`.`name` LEFT JOIN `dropdown_settings_option` ON `order`.`order_status`= `dropdown_settings_option`.`option_value` LEFT JOIN `order_item` ON `order`.`id`= `order_item`.`order_id` ORDER BY `order`.`id` DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT })
                    .then(function (value) {
                        orderStatus = sequelize.query("SELECT dropdown_settings_option.id as optionID, dropdown_settings_option.option_value as optionValue, dropdown_settings_option.option_label as optionLabel, dropdown_settings.identifier as dropIdentifier FROM dropdown_settings_option LEFT JOIN dropdown_settings on dropdown_settings_option.dropdown_id = dropdown_settings.id where dropdown_settings.identifier='order_status'",{ type: Sequelize.QueryTypes.SELECT })  
                        orderStatus.then(function (orderStatus) {
                            const itemCount = ordercount.length > 0 ? ordercount[0].orderCount : 0;
                            const pageCount =  ordercount.length > 0 ? Math.ceil(ordercount[0].orderCount / limit) : 1;
                            const previousPageLink = paginate.hasNextPages(req)(pageCount);
                            const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
                            const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                        
                            return res.render('superpos/order/list', { title: 'Order',arrData:value,arrOrderStatus:orderStatus,arrOption:'',message:'',errors:'',		
                                pageCount,
                                itemCount,
                                currentPage: currPage,
                                previousPage : previousPageLink	,
                                startingNumber: startItemsNumber,
                                endingNumber: endItemsNumber,
                                limit : limit ,
                                pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
                            });
                        });
                    }); 
                }else{
                    return res.render('superpos/order/list', { title: 'Order',arrData:'',arrOrderStatus:'',arrOption:'',message:'',errors:'',		
                        pageCount:0,
                        itemCount:0,
                        currentPage: currPage,
                        previousPage : previousPageLink	,
                        startingNumber: startItemsNumber,
                        endingNumber: endItemsNumber,
                        limit : limit ,
                        pages: paginate.getArrayPages(req)(limit, pageCount, currPage,keyword)	
                    });
                }                   
            });
        }	
    });	  
    // var arrData = null;
    // fetch(req.app.locals.apiurl+'order',{headers: {
    //     "Content-Type": "application/json; charset=utf-8",
    //     "token": req.session.token,
        
    // }}) .then(function(response) { return response.json() })
    // .then(function(data){
    //     //return res.send(data);
	// 	return res.render('superpos/order/list', { title: 'Order',arrData:data.value,arrOrderStatus:data.orderStatus,arrOption:'',message:'',errors:''		
	// 	});
	// });
}

// var currPage = req.query.page ? req.query.page : 0;
// var limit = req.query.limit ? req.query.limit : 5;
// var offset = currPage!=0 ? (currPage * limit) - limit : 0;
// var token= req.session.token;
//     jwt.verify(token, SECRET, function(err, decoded) {
//         if (err) {
//             res.status(200).send({data:{verified:false},errNode:{errMsg:"Invalid Token",errCode:"1"}});
//         }else{
//             existingItem = models.order.findAndCountAll({limit: limit, offset: offset});            
//             existingItem.then(function (results) {
//                 const itemCount = results.count;
//                 const pageCount = Math.ceil(results.count / limit);
//                 const previousPageLink = paginate.hasNextPages(req)(pageCount);
//                 const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
//                 const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
//                 console.log(startItemsNumber);
//                 console.log(endItemsNumber);

//                 // console.log(previousPageLink)
//                     return res.render('superpos/order/list', { title: 'Order',arrData:results.value,arrOption:'',message:'',arrData:results.rows,errors:'',		
//                     pageCount,
//                     itemCount,
//                     currentPage: currPage,
//                     previousPage : previousPageLink	,
//                     startingNumber: startItemsNumber,
//                     endingNumber: endItemsNumber,
//                     pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
//                 }); 
//             })
//         }	
//     });
//}


exports.addeditOrder = function(req, res, next){    
    var id = req.params.id;
    var pages = req.params.page;
   // return res.send(pages)
    if(!id){
        fetch(req.app.locals.apiurl+'order/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){

            // return res.send(value);
            return res.render('superpos/order/addedit', {title: 'Add order',arrData:'',arrOption:'',messages: req.flash('info'),arrStore: value.arrstores,arrCustomer: value.arrcustomer, arrSalesman: value.arrsalesman, arrTimeSlot: value.arrTimeSlot,  arrOrderStatus:value. arrOrderStatus, arrShippingMethod : value.arrShippingMethod, arrPaymentMethod: value.arrPaymentMethod, arrCategory :value.arrCategory, arrOrderItems: '', page: pages, errors:''});
        });
    }else{            
        fetch(req.app.locals.apiurl+'order/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
           // console.log("Llllllllllllllllllllllllllllllllllllllllllllllll")
            // return res.send(value)
            return res.render('superpos/order/addedit', {title: 'Edit order',arrData: value.value,arrOption: value.att_value,messages: req.flash('info'),arrStore: value.arrstores,arrCustomer: value.arrcustomer, arrSalesman: value.arrsalesman, arrTimeSlot: value.arrTimeSlot,  arrOrderStatus:value. arrOrderStatus, arrShippingMethod : value.arrShippingMethod, arrPaymentMethod: value.arrPaymentMethod, arrCategory :value.arrCategory, arrOrderItems: value.arrOrderItems, page: pages, errors:''});
        });
    }	
};






exports.updateOrder =async function(req, res, next){
   

    var id = req.body.update_id;
    var page =  req.body.page_number;
   // return res.send(page);
    var result =await models.order.findOne({where:{id:id}})
    var status1 = result.order_status;
// console.log("+++++++++++++++++++++++++++++++"+id);
// console.log("+++++++++++++++++++++++++++++++"+ req.body.wallet_amount);
// console.log("+++++++++++++++++++++++++++++++"+ req.body.wallet_amount);
// console.log("+++++++++++++++++++++++++++++++"+ req.body.payment_method);
    await models.order.update({ 
            store_id: req.body.store,
            order_status: req.body.order_status,
            shipping_method: req.body.shipping_method,
            shipping_description: req.body.shipping_description,
            //payment_method: req.body.payment_method, 
            salesman_id : req.body.salesman_id ,
            base_grand_total: req.body.base_grand_total?req.body.base_grand_total:null,
            shipping_amount:req.body.shipping_amount?parseFloat(req.body.shipping_amount): 0,
            grand_total:req.body.base_total,
            street1:req.body.street1,
            street2:req.body.street2,
            city:req.body.city,
            state:req.body.state,
            pin:req.body.pin,
            country:req.body.country,
            lat:req.body.lat,
            long:req.body.long,
            gift_message:req.body.gift_message,
            remote_ip:req.body.remote_ip,
            medium:req.body.medium,                
            amount_paid:req.body.amount_paid_total,
            coupon_amount:req.body.coupon_amount?req.body.coupon_amount:null,
            wallet_amount:req.body.wallet_amount?req.body.wallet_amount:null,
        },{where:{id:id}}).then(async function(order) {
       // console.log("333333333333333333333333333333333333333333333l")
      

           if(status1 == req.body.order_status){

              req.flash('info','Successfully Updated');
              return res.redirect('/superpos/order?page='+page+'&limit=10');
          }else{
            
              var order_status_data = req.body.order_status;

              console.log(order_status_data);
              // return res.send(order_status_data);
              // console.log(order_status_data);
              if(!id){
                  res.status(200).send({ status:205, message: "Id not found" });
              }else{
                  // res.status(200).send({ status:200,value:'' });
                  if(order_status_data == 'Delivered'){
                
                          models.order.findById(id).then(function(order_details) {
                              models.customer.findById(order_details.customer_id).then(function(customer_details) {                       
                                  var smsContent = 'Your order with the reference no. '+order_details.order_id+' has been delivered. Thank you, we hope to see you soon. Contact us at +91 9232456754'
                                  sms_controller.sendsms(customer_details.phone,smsContent);
                                  models.order_details.create({ 
                                      order_details: JSON.stringify(order_details)
                                  }).then(async function(order_details_data) {
          
                                
                                     
                                  })
                                  .catch(function(error) {
                                      res.status(200).send({ status:200, success: false,  message: "Archive Failed" });
                                  });
                              })
                              .catch(function(error) {
                                  res.status(200).send({ status:200, success: false,  message: "Customer Not Found" });
                              });
                          })
                          .catch(function(error) {
                          res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                      });
                  }else if(order_status_data == 'Shipped'){
                     
                          models.order.findById(id).then(function(order_details) {
                           models.salesman.findOne({where:{id:order_details.salesman_id}}).then(function(selsman){
                              models.customer.findById(order_details.customer_id).then(function(customer_details) {
                                  var smsContent = 'Your order with the reference no. '+order_details.order_id+' has been shipped and will be delivered by '+selsman.name+'('+selsman.phone+').'
                                  sms_controller.sendsms(customer_details.phone,smsContent);
                                  models.order_details.create({ 
                                      order_details: JSON.stringify(order_details)
                                  }).then(async function(order_details_data) {
                                     
                                  
                                  })
                                  .catch(function(error) {
                                      res.status(200).send({ status:200, success: false,  message: "Archive Failed" });
                                  });
                              })
                              .catch(function(error) {
                                  res.status(200).send({ status:200, success: false,  message: "Customer Not Found" });
                              });
                           })
                            .catch(function(error) {
                                  res.status(200).send({ status:200, success: false,  message: "Salesman Not Found" });
                              });
                          })
                         .catch(function(error) {
                          res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                      });
          
                  }else if(order_status_data == 'Canceled'){
                     
                          models.order.findById(id).then(function(order_details) {
                              // console.log(order_details.customer_id)
                              models.customer.findById(order_details.customer_id).then(function(customer_details) {
                                  // console.log(customer_details.phone)
                                  var smsContent = 'Your order with the reference no. '+order_details.order_id+' has been canceled. Thank you'
                                  sms_controller.sendsms(customer_details.phone,smsContent);
                                  models.order_details.create({ 
                                      order_details: JSON.stringify(order_details)
                                  }).then(async function(order_details_data) {
          
                                    
                                  })
                                  .catch(function(error) {
                                      res.status(200).send({ status:200, success: false,  message: "Archive Failed" });
                                  });
                              })
                              .catch(function(error) {
                                  res.status(200).send({ status:200, success: false,  message: "Customer Not Found" });
                              });
                          })
                         .catch(function(error) {
                          console.log(error)
                          res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                      });
          
                  }else{
                      models.order.update({
                          //customer_id: customerId,
                          order_status:order_status_data,
                      },{where:{id:id}}).then(function(value) {
                          models.order_details.create({ 
                              order_details: JSON.stringify(order_details)
                          }).then(function(order_details_data) {
                              // console.log(order_details_data)
                              res.status(200).send({ status:200, success: true,  value:value });
                          })
                          .catch(function(error) {
                              res.status(200).send({ status:200, success: false,  message: "Archive Failed" });
                          });
                      }).catch(function(error) {
                          res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
                      });
                  }
                 
              }
          
          
              req.flash('info','Successfully Updated');
              return res.redirect('back');



          };

   });

           
            product_id = req.body.product_id;
            quantity = req.body.qty;
            price = req.body.price;
           var count = product_id.length;
       


           if(typeof(price) == 'object' ){


          
            for(var i=0; i<count; i++) {
                console.log("**************"+i);

              await models.order_item.update({

                qty:quantity[i],
                original_price:price[i]
               
              },{where:
                {order_id:id,
                product_id:product_id[i] } 
            }) ;
               
            };
            }else{


                await models.order_item.update({

                    qty:quantity,
                    original_price:price
                   
                  },{where:
                    {order_id:id,
                    product_id:product_id } 
                }) ;
                   


            }
             //console.log("-----------------------------------------------------------++++++++++++++");


};
    



    
            

exports.deleteOrder = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'order/delete/'+id,{headers: {
      "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })	
}

exports.orderStatusChange = function(req, res, next) {
    var id = req.params.id;
    var order_status_data = req.params.data;
    console.log(order_status_data);
    // return res.send(order_status_data);
    // console.log(order_status_data);
    if(!id){
        res.status(200).send({ status:205, message: "Id not found" });
    }else{
        // res.status(200).send({ status:200,value:'' });
        if(order_status_data == 'Delivered'){
            models.order.update({
                //customer_id: customerId,
                order_status:order_status_data,
            },{where:{id:id}}).then(function(value) {
                models.order.findById(id).then(function(order_details) {
					models.customer.findById(order_details.customer_id).then(function(customer_details) {                       
                        var smsContent = 'Your order with the reference no. '+order_details.order_id+' has been delivered. Thank you, we hope to see you soon. Contact us at +91 9232456754'
                        sms_controller.sendsms(customer_details.phone,smsContent);
						models.order_details.create({ 
							order_details: JSON.stringify(order_details)
						}).then(async function(order_details_data) {

                      
                            var contact = await helpers.getEmailConfig();
                            var email =contact[1];
                            var phoneno =contact[0];
                          
							// console.log(order_details_data)
							return new Promise((resolve, reject) => {
								var data = {
									from: 'Grocery User <me@samples.mailgun.org>',
                                    // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                    to: ['subrata.adhikary110319@gmail.com'],
									subject: 'Delivering Order',
									html: '<!DOCTYPE html>'+
									'<html>'+    
									'<head>'+
									'</head>'+    
									'<body>'+
										'<div>'+
											'<p>Hello Sir/Madam,</p>'+
											'<p>Your order with the reference no. '+order_details.order_id+' has been delivered.</p>'+  
											'<p>For any kind of query reach us at: <br />'+
											'Mail: " '+email+' " <br />' +
											'Phone: " '+phoneno+' " <br />'+
											'Have a nice shopping.</p>'+
											'<p>Thanks.</p>'+
											'<p>Regards,<br />'+
											'One Tap Grocery<br />'+
											'Midnapore</p>'+								
										'</div>'+       
									'</body>'+    
									'</html>' 
								};
								mailgun.messages().send(data, function (error, body) {
									if (error) {
										return reject(res.status(200).send({ status:200, success: true,  value:value }));
									}
									return resolve(res.status(200).send({ status:200, success: true,  value:value }));
								});
							});
						})
						.catch(function(error) {
							res.status(200).send({ status:200, success: false,  message: "Archive Failed" });
						});
					})
                    .catch(function(error) {
                        res.status(200).send({ status:200, success: false,  message: "Customer Not Found" });
                    });
                })
                .catch(function(error) {
                    res.status(200).send({ status:200, success: false,  message: "Order Delivary failed" });
                });
                
            }).catch(function(error) {
                res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
            });
        }else if(order_status_data == 'Shipped'){
            models.order.update({
                //customer_id: customerId,
                order_status:order_status_data,
            },{where:{id:id}}).then(function(value) {
                models.order.findById(id).then(function(order_details) {
                 models.salesman.findOne({where:{id:order_details.salesman_id}}).then(function(selsman){
					models.customer.findById(order_details.customer_id).then(function(customer_details) {
                        var smsContent = 'Your order with the reference no. '+order_details.order_id+' has been shipped and will be delivered  by '+selsman.name+'('+selsman.phone+').'
                        sms_controller.sendsms(customer_details.phone,smsContent);
						models.order_details.create({ 
							order_details: JSON.stringify(order_details)
						}).then(async function(order_details_data) {
                           
                            var contact = await helpers.getEmailConfig();
                            var email =contact[1];
                            var phoneno =contact[0];

							// console.log(order_details_data)
							return new Promise((resolve, reject) => {
								var data = {
									from: 'Grocery User <me@samples.mailgun.org>',
                                    // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                    to: ['subrata.adhikary110319@gmail.com'],
									subject: 'Shipping Order',
									html: '<!DOCTYPE html>'+
									'<html>'+    
									'<head>'+
									'</head>'+    
									'<body>'+
										'<div>'+
											'<p>Hello Sir/Madam,</p>'+
											'<p>Your order with the reference no. '+order_details.order_id+' has been shipped and will be delivered soon.</p>'+  
											'<p>For any kind of query reach us at: <br />'+
											'Mail:" '+email+' "<br />'+
											'Phone:" '+phoneno+' "<br />'+
											'Have a nice shopping.</p>'+
											'<p>Thanks.</p>'+
											'<p>Regards,<br />'+
											'One Tap Grocery<br />'+
											'Midnapore</p>'+								
										'</div>'+       
									'</body>'+    
									'</html>' 
								};
								mailgun.messages().send(data, function (error, body) {
									if (error) {
										return reject(res.status(200).send({ status:200, success: true,  value:value }));
									}
									return resolve(res.status(200).send({ status:200, success: true,  value:value }));
								});
							});
						})
						.catch(function(error) {
							res.status(200).send({ status:200, success: false,  message: "Archive Failed" });
						});
                    })
                    .catch(function(error) {
                        res.status(200).send({ status:200, success: false,  message: "Customer Not Found" });
                    });
                 })
                  .catch(function(error) {
                        res.status(200).send({ status:200, success: false,  message: "Salesman Not Found" });
                    });
                })
                .catch(function(error) {
                    res.status(200).send({ status:200, success: false,  message: "Order Shipment failed" });
                });
                
            }).catch(function(error) {
                res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
            });

        }else if(order_status_data == 'Canceled'){
            models.order.update({
                //customer_id: customerId,
                order_status:order_status_data,
            },{where:{id:id}}).then(function(value) {
                models.order.findById(id).then(function(order_details) {
                    // console.log(order_details.customer_id)
                    models.customer.findById(order_details.customer_id).then(function(customer_details) {
                        // console.log(customer_details.phone)
                        var smsContent = 'Your order with the reference no. '+order_details.order_id+' has been canceled. Thank you'
                        sms_controller.sendsms(customer_details.phone,smsContent);
						models.order_details.create({ 
							order_details: JSON.stringify(order_details)
						}).then(async function(order_details_data) {

                            var contact = await helpers.getEmailConfig();
                            var email =contact[1];
                            var phoneno =contact[0];

							console.log(order_details_data)
							return new Promise((resolve, reject) => {
								var data = {
									from: 'Grocery User <me@samples.mailgun.org>',
                                    // to: ['tbiswa1993@gmail.com','skabdul.barik108@gmail.com'],
                                    to: ['subrata.adhikary110319@gmail.com'],
									subject: 'Cancel Order',
									html: '<!DOCTYPE html>'+
									'<html>'+    
									'<head>'+
									'</head>'+    
									'<body>'+
										'<div>'+
											'<p>Hello Sir/Madam,</p>'+
											'<p>Your order with the reference no. '+order_details.order_id+' has been canceled.</p>'+  
											'<p>For any kind of query reach us at: <br />'+
											'Mail: " '+email+'" <br />'+
											'Phone: " '+phoneno+'"<br />'+
											'Have a nice shopping.</p>'+
											'<p>Thanks.</p>'+
											'<p>Regards,<br />'+
											'One Tap Grocery<br />'+
											'Midnapore</p>'+								
										'</div>'+       
									'</body>'+    
									'</html>' 
								};
								mailgun.messages().send(data, function (error, body) {
									if (error) {
										return reject(res.status(200).send({ status:200, success: true,  value:value }));
									}
									return resolve(res.status(200).send({ status:200, success: true,  value:value }));
								});
							});
						})
						.catch(function(error) {
							res.status(200).send({ status:200, success: false,  message: "Archive Failed" });
						});
					})
                    .catch(function(error) {
                        res.status(200).send({ status:200, success: false,  message: "Customer Not Found" });
                    });
                })
                .catch(function(error) {
                    res.status(200).send({ status:200, success: false,  message: "Order Cancellation failed" });
                });
                
            }).catch(function(error) {
                console.log(error)
                res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
            });

        }else{
            models.order.update({
                //customer_id: customerId,
                order_status:order_status_data,
            },{where:{id:id}}).then(function(value) {
                models.order_details.create({ 
                    order_details: JSON.stringify(order_details)
                }).then(function(order_details_data) {
                    // console.log(order_details_data)
                    res.status(200).send({ status:200, success: true,  value:value });
                })
                .catch(function(error) {
                    res.status(200).send({ status:200, success: false,  message: "Archive Failed" });
                });
            }).catch(function(error) {
                res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
            });
        }
        // models.order.update({
        //     //customer_id: customerId,
        //     order_status:order_status_data,
        // },{where:{id:id}}).then(function(value) {
        //     res.status(200).send({ status:200,value:value });
        // }).catch(function(error) {
        //     res.status(400).send({data:{verified:false},errNode:{errMsg:error,errCode:"1"}});
        // });
    }
};


exports.otgInvoice = function(req, res, next){    
    var id = req.params.id;
    //if(!id){
        // fetch(req.app.locals.apiurl+'order/addedit',{headers: {
        //     "Content-Type": "application/json; charset=utf-8",
        //     "token": req.session.token,
        // }}) .then(function(response) { return response.json() })
        // .then(function(value){

            // return res.send(value);

            
        models.order.findOne({ where: {id:id} })
		.then(function (order) {
        // sequelize.query("SELECT `order`.*, `customer`.`first_name` as `customer_first_name`, `customer`.`last_name` as `customer_last_name`, `customer`.`email` as `customerEmail` FROM `order` LEFT JOIN `customer` ON `order`.`customer_id`= `customer`.`id` where `order`.id ="+id,{ type: Sequelize.QueryTypes.SELECT })
        // .then(function(order) {     
            // models.order_item.findAll({ where: {order_id:id} })
            // .then(function (order_item) {
            sequelize.query("select order_item.*, product.weight as productWeight FROM `order_item` LEFT JOIN `product` ON product.id = order_item.product_id where order_item.order_id ="+id,{ type: Sequelize.QueryTypes.SELECT })
            .then(function(order_item) {  
                var p=0; 
                for(var i=0;i<order_item.length;i++)
                {
                
                p = p+order_item[i].qty;
            }
            
                return res.render('superpos/order/otg_invoice', {title: 'Add order', arrOrder:order, arrOrderItem:order_item, arrorderproduct: order_item.length, totalqty:p});
            });
        });
    //}	
};
// exports.orderSearch =function(req, res) {
//     var currPage = req.query.page ? req.query.page : 0;
//     var limit = req.query.limit ? req.query.limit : 10;
//     var offset = currPage!=0 ? (currPage * limit) - limit : 0;
//     console.log(req.params.data);
//     var keyword;
//     var keyword = req.params.data;
//     //keyword = req.body.search;
//     console.log("+++++");
//     console.log(keyword);

  
//    sequelize.query("SELECT COUNT(*) AS orderCount FROM `order`where customer_name like  '%"+keyword+"%' or customer_mobile like  '%"+keyword+"%' or order_id like  '%"+keyword+"%' or shipping_address like  '%"+keyword+"%' or grand_total like  '%"+keyword+"%'  ",{ type: Sequelize.QueryTypes.SELECT })
//             .then(function (ordercount) {
//                 // console.log(ordercount[0].orderCount)
//                 if(ordercount ){
                    
//                     sequelize.query("SELECT `order`.*,  (SELECT COUNT(*) FROM order_item WHERE  order_item.order_id = order.id)  as `numberOfOrder`, `dropdown_settings_option`.`option_label` as `orderStatusTitle`, `shipping_method`.`price` as `ShippingPrice` FROM `order` LEFT JOIN `shipping_method`  ON `order`.`shipping_method`= `shipping_method`.`name` LEFT JOIN `dropdown_settings_option` ON `order`.`order_status`= `dropdown_settings_option`.`option_value`  where order.customer_name like  '%"+keyword+"%' or order.customer_mobile like  '%"+keyword+"%' or order.order_id like  '%"+keyword+"%' or order.shipping_address like  '%"+keyword+"%' or order.grand_total like  '%"+keyword+"%'   ORDER BY `order`.`id` DESC LIMIT "+offset+", "+limit,{ type: Sequelize.QueryTypes.SELECT })
                   
//                     .then(function (value) {
//                         orderStatus = sequelize.query("SELECT dropdown_settings_option.id as optionID, dropdown_settings_option.option_value as optionValue, dropdown_settings_option.option_label as optionLabel, dropdown_settings.identifier as dropIdentifier FROM dropdown_settings_option LEFT JOIN dropdown_settings on dropdown_settings_option.dropdown_id = dropdown_settings.id where dropdown_settings.identifier='order_status'",{ type: Sequelize.QueryTypes.SELECT })  
//                         orderStatus.then(function (orderStatus) {
//                             const itemCount = ordercount.length > 0 ? ordercount[0].orderCount : 0;
//                             const pageCount =  ordercount.length > 0 ? Math.ceil(ordercount[0].orderCount / limit) : 1;
//                             const previousPageLink = paginate.hasNextPages(req)(pageCount);
//                             const startItemsNumber = currPage== 0 || currPage==1 ? 1 : (currPage - 1) * limit +1;
//                             const endItemsNumber = pageCount== currPage ||  pageCount== 1 ? itemCount : currPage * limit ;
                        
//                             return res.render('superpos/order/searchOrderlist', { title: 'Search Result',arrData:value,arrOrderStatus:orderStatus,arrOption:'',message:'',errors:'',		
//                                 pageCount,
//                                 itemCount,
//                                 currentPage: currPage,
//                                 previousPage : previousPageLink	,
//                                 startingNumber: startItemsNumber,
//                                 endingNumber: endItemsNumber,
//                                 pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
//                             });
//                         });
//                     }); 
//                 }else{
//                     return res.render('superpos/order/searchOrderlist', { title: 'Search Result',arrData:'',arrOrderStatus:'',arrOption:'',message:'',errors:'',		
//                         pageCount:0,
//                         itemCount:0,
//                         currentPage: currPage,
//                         previousPage : previousPageLink	,
//                         startingNumber: startItemsNumber,
//                         endingNumber: endItemsNumber,
//                         pages: paginate.getArrayPages(req)(limit, pageCount, currPage)	
//                     });
//                 }                   
//             });
//         }	


       