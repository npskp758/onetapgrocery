
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');
var fetch = require('node-fetch');


exports.cmsList = function(req, res, next){
	  
    var arrData = null;
    fetch(req.app.locals.apiurl+'cms',{headers: {
        "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
    }}) .then(function(response) { return response.json() })
    .then(function(value){
		return res.render('superpos/cms/list', { title: 'CMS',arrData: value.value,arrOption:'',message: req.flash('message'),errors:''		
		});
	});
}


exports.addeditCms = function(req, res, next){
    
    var id = req.params.id;
    var arrData = null;
    var arrOption = null;
    var arrStore = null;
    if(!id){
        fetch(req.app.locals.apiurl+'cms/addedit',{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            return res.render('superpos/cms/addedit', {title: 'Add CMS',arrData:'',arrOption:'',messages: req.flash('info'),arrStore: value.stores,errors:''});
        });
    }else{            
        fetch(req.app.locals.apiurl+'cms/addedit/'+id,{headers: {
            "Content-Type": "application/json; charset=utf-8",
            "token": req.session.token,
        }}) .then(function(response) { return response.json() })
        .then(function(value){
            //res.send(value.att_value);	
            return res.render('superpos/cms/addedit', {title: 'Edit CMS',arrData: value.value,arrOption: value.att_value,messages: req.flash('info'),arrStore: value.stores,errors:''});

            });
    }	
};

exports.deleteCms = function(req, res, next){
	var id = req.params.id;	
	console.log(id)
    fetch(req.app.locals.apiurl+'cms/delete/'+id,{headers: {
      "Content-Type": "application/json; charset=utf-8",
        "token": req.session.token,
	}}).then(function(response) { res.redirect('back'); })	
}

  