
var models = require('../../models');
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
var formidable = require('formidable');
var multiparty = require('multiparty'); 
var bodyParser = require('body-parser');


// var multer = require('multer');
// var storage = multer.diskStorage({
// destination: function (req, file, cb) {
// cb(null, 'public/images/')
// },
// filename: function (req, file, cb) {
// 	console.log(req.files);
// cb(null, file.originalname + '-' + Date.now()+ '.' +file.originalname.split('.').pop()) //Appending extension
// }
// })
// var upload = multer({ storage: storage });


exports.dashboard = function(req, res, next){
	  
	res.render('superpos/home/dashboard');
}



exports.modulesList = function(req, res, next){
	var arrData = null;
	// console.log(__dirname);
	// return res.send(__dirname);
	existingItem = models.modules.findAll({ where: {status: {$ne: 'archive'}} }); 
		existingItem.then(function (value) {
		return res.render('superpos/modules/list', {
			title: 'Modules',
			arrData: value,
			messages: req.flash('message'),
			errors: req.flash('errors')	
		});

	});
}


exports.addedit = function(req, res, next){
  var id = req.params.id;
	var existingItem = null;
	var headerItem = null;
	var nineiconsItem = null;
	var headerCount = 0;
	var nineiconsCount = 0;

	headerItem = models.modules.findAll({ where: {status:'active',usefor:'header'}  }); 
		
		headerItem.then(function (head) {
			head.forEach(function(mod) { 
				headerCount++;
			});

	nineiconsItem = models.modules.findAll({ where: {status:'active',usefor:'nineicons'}  }); 	
	
			nineiconsItem.then(function (nine) {
			nine.forEach(function(item) { 
				nineiconsCount++;
			});

    if(!id){	
	res.render('superpos/modules/addedit',{title:'Modules',arrData:'',nineiconsCount: nineiconsCount,headerCount: headerCount,messages: req.flash('message'),
		errors: req.flash('errors')	});	
	res.render('superpos/modules/addedit',{title:'Modules',arrData:'',messages: req.flash('message'),errors: req.flash('errors')});	
		}else{
			console.log(id);
	existingItem = models.modules.findOne({ where: {id:id} });
	existingItem.then(function (value) {		
    return res.render('superpos/modules/addedit', {
        title: 'Modules',
		arrData: value,
		headerCount: headerCount,
		nineiconsCount: nineiconsCount,
		messages: req.flash('message'),
		errors: req.flash('errors')	
		});
	});
	
}
});
});
}


exports.add = function(req, res, next) {
	var data = req.body;
	var id = data.update_id;
		var d = new Date();
		var n = d.getTime();
	

	var form = new multiparty.Form(); 
	form.parse(req, function(err, fields, files) { 
		// console.log(fields);
		// console.log(files);
		// return res.send(fields);
		var id = fields.update_id[0];

		var m_menuIcon=null;
		if(files.menuIcon[0].originalFilename!=''){
			m_menuIcon=req.app.locals.baseurl+'superpos/myimages/'+n+files.menuIcon[0].originalFilename;
		}else{
			m_menuIcon=fields.update_menuIcon[0];
		}
		var m_menuHoverIcon=null;
		if(files.menuHoverIcon[0].originalFilename!=''){
			m_menuHoverIcon=req.app.locals.baseurl+'superpos/myimages/'+n+files.menuHoverIcon[0].originalFilename;
		}else{
			m_menuHoverIcon=fields.update_menuHoverIcon[0];
		}
		var m_homeIcon=null;
		if(files.homeIcon[0].originalFilename!=''){
			m_homeIcon=req.app.locals.baseurl+'superpos/myimages/'+n+files.homeIcon[0].originalFilename;
		}else{
			m_homeIcon=fields.update_homeIcon[0];
		}
		var m_bannerImage=null;
		if(files.bannerImage[0].originalFilename!=''){
			m_bannerImage=req.app.locals.baseurl+'superpos/myimages/'+n+files.bannerImage[0].originalFilename;
		}else{
			m_bannerImage=fields.update_bannerImage[0];
		}
		var m_bannerIcon=null;
		if(files.bannerIcon[0].originalFilename!=''){
			m_bannerIcon=req.app.locals.baseurl+'superpos/myimages/'+n+files.bannerIcon[0].originalFilename;
		}else{
			m_bannerIcon=fields.update_bannerIcon[0];
		}
		var m_footerImage1=null;
		if(files.footerImage1[0].originalFilename!=''){
			m_footerImage1=req.app.locals.baseurl+'superpos/myimages/'+n+files.footerImage1[0].originalFilename;
		}else{
			m_footerImage1=fields.update_footerImage1[0];
		}
		var m_footerImage2=null;
		if(files.footerImage2[0].originalFilename!=''){
			m_footerImage2=req.app.locals.baseurl+'superpos/myimages/'+n+files.footerImage2[0].originalFilename;
		}else{
			m_footerImage2=fields.update_footerImage2[0];
		}

		// var m_slag=null;
		// m_slag=fields.slag[0];
		// //var m_slag1 = m_slag.replace(" ", "-");
		// var m_slag1 = m_slag.split(' ').join('-');

		req.body = fields;
		req.checkBody('name', 'Name Field Required').notEmpty();
		//req.checkBody('slag', 'Slag Name Required').notEmpty();
		

		req.getValidationResult().then(function(result){
       if(!result.isEmpty()){
		   var errors = result.array().map(function (elem) {
                return elem.msg;
            });
            //console.log('There are following validation errors: ' + errors.join('&&'));
           req.flash('errors',errors);
		   res.redirect('back');
		   }else{

			var duplicate = null;
			//console.log('ok');
				duplicate = models.modules.findAll({ where: {id: {$ne: id},name:fields.name[0],status:{$ne:'archive'}} });
				duplicate.then(function (duplicate) {
				if(duplicate.length){
					//console.log('okkkkkkk');
					req.flash('errors',' This Name Is Already Exists');  
					res.redirect('back');	
				}else{



		if(!id){

			var slug=slugify(fields.name[0]);	
			models.modules.create({ 
				name: fields.name[0]?fields.name[0]:null, 
				slag:slug,
				usefor: fields.usefor[0]?fields.usefor[0]:null,
				relatedTableName: fields.relatedTableName[0]?fields.relatedTableName[0]:null,
				menuIcon:m_menuIcon?m_menuIcon:null,
				menuHoverIcon:m_menuHoverIcon?m_menuHoverIcon:null,
				homeIcon:m_homeIcon?m_homeIcon:null,
				bannerImage:m_bannerImage?m_bannerImage:null,
				bannerIcon:m_bannerIcon?m_bannerIcon:null,
				subTitle: fields.subTitle[0]?fields.subTitle[0]:null,
				shortDesc: fields.shortDesc[0]?fields.shortDesc[0]:null,
				desc: fields.desc[0]?fields.desc[0]:null,
				footerImage1:m_footerImage1?m_footerImage1:null,
				footerImage2:m_footerImage2?m_footerImage2:null,
				footerContent1: fields.footerContent1[0]?fields.footerContent1[0]:null, 
				footerContent2: fields.footerContent2[0]?fields.footerContent2[0]:null, 
				metaTitle: fields.metaTitle[0]?fields.metaTitle[0]:null, 
				metaDesc: fields.metaDesc[0]?fields.metaDesc[0]:null, 
				metaKeyword: fields.metaKeyword[0]?fields.metaKeyword[0]:null, 
				sequence: fields.sequence[0]?fields.sequence[0]:null,
				createdBy:fields.user_id[0]?fields.user_id[0]:null,
				updatedBy:"",
				status: fields.status[0]?fields.status[0]:null
					}).then(function(modules) {

						//return res.send('user created');
					req.flash('message','Successfully Created');	
					res.redirect('back');
						
			
				})
		}else{
			//console.log(fields);
			//return res.send(fields);

			models.modules.update({ 
				name: fields.name[0]?fields.name[0]:null, 
				//slag:m_slag1,
				usefor: fields.usefor[0]?fields.usefor[0]:null,
				relatedTableName: fields.relatedTableName[0]?fields.relatedTableName[0]:null,
				menuIcon:m_menuIcon?m_menuIcon:null,
				menuHoverIcon:m_menuHoverIcon?m_menuHoverIcon:null,
				homeIcon:m_homeIcon?m_homeIcon:null,
				bannerImage:m_bannerImage?m_bannerImage:null,
				bannerIcon:m_bannerIcon?m_bannerIcon:null,
				subTitle: fields.subTitle[0]?fields.subTitle[0]:null,
				shortDesc: fields.shortDesc[0]?fields.shortDesc[0]:null,
				desc: fields.desc[0]?fields.desc[0]:null,
				footerImage1:m_footerImage1?m_footerImage1:null,
				footerImage2:m_footerImage2?m_footerImage2:null,
				footerContent1: fields.footerContent1[0]?fields.footerContent1[0]:null, 
				footerContent2: fields.footerContent2[0]?fields.footerContent2[0]:null, 
				metaTitle: fields.metaTitle[0]?fields.metaTitle[0]:null, 
				metaDesc: fields.metaDesc[0]?fields.metaDesc[0]:null, 
				metaKeyword: fields.metaKeyword[0]?fields.metaKeyword[0]:null, 
				sequence: fields.sequence[0]?fields.sequence[0]:null,
				//createdBy:1,
				updatedBy:fields.user_id[0]?fields.user_id[0]:null,
				status: fields.status[0]?fields.status[0]:null
					},{where:{id:id}}).then(function(modules) {
						

					req.flash('message','Successfully Updated');	  
					res.redirect('back');
		   });	
		   }
	
						
						//return res.send('user created');
					// req.flash('message','Updated Successfully');	
					// res.redirect('back');

					}

								});
							}
						
					
			});		
		
	});		
	


	//form.parse(req, function(err, fields, files) { 
		//if(files.length >0){
		var formnew = new formidable.IncomingForm();
		
		formnew.parse(req);

		formnew.on('fileBegin', function (name, file){
			//return res.send(file);
			if(file.name && file.name!=''){
				file.path = __dirname + '/../../public/superpos/myimages/'+n+ file.name;
			}	
		});
		//}
	//});
	
	
};

exports.deleteModules = function(req, res, next) {
	var data = req.body;
	var id = req.params.id;
	
	
				
			models.modules.update({ 
				status:'archive',
				updatedBy:1
				 },{where:{id:id}}).then(function(modules) {
				
			req.flash('message','Successfully Deleted');	  
           res.redirect('back');
				});	
				
	
};

function slugify(text) {
	 return text.toString().toLowerCase() .replace(/\s+/g, '-') 
	 // Replace spaces with - .replace(/[^\w\-]+/g, '')
	 // Remove all non-word chars .replace(/\-\-+/g, '-') // Replace multiple - with single - .replace(/^-+/, '') 
}