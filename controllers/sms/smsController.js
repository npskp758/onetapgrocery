const smsConfig = require('../../config/sms-config')();
var Client = require('node-rest-client').Client;

exports.sendsms = function(number, message) {
    console.log(number);
    var client = new Client();
 
    var args = {
        requesConfig: { timeout: 1000 },
        responseConfig: { timeout: 2000 }
    };
    // &mobiles=+9187592929&sms=test sms&senderid=abcdef
    // direct way
    var req1 = client.get(smsConfig.api+"&mobiles="+number+"&sms="+message+"&senderid=OTGLTD&accusage=1", function (data, response) {
        // parsed response body as js object
        console.log('data');
        console.log(data);
        // raw response
        // console.log('response');
        // console.log(response);
    });
    
    // view req1 options	
    // console.log('req1.options');
    // console.log(req1.options);
    
    
    req1.on('requestTimeout', function (req) {
        console.log("request has expired");
        req.abort();
    });
    
    req1.on('responseTimeout', function (res) {
        console.log("response has expired");
    
    });
    
    
    // handling specific req2 errors
    req1.on('error', function (err) {
        console.log('something went wrong on req1!!', err.request.options);
    });
}